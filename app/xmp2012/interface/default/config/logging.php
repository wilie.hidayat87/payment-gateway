<?php

class config_logging {

    public $timeDigit = 8;

    public $lineFormat = "{uniqueId} {level} {datetime} {exectime} {class} {function} {message} {response}";
    //public $loglevel = 2; //1; //set to 9 to disable debug level logging
    public $loglevel = 1;

    public $profile = array(
        'default' => array(
            'path' => '/backuplog/logs/simplepay/default',
            'type' => 'file',
            'filename' => 'default'
        )
    );

}

<?php

class config_logging {

    public $timeDigit = 8;

    public $lineFormat = "{uniqueId} {level} {datetime} {exectime} {class} {function} {message} {response}";
    //public $loglevel = 2; //1; //set to 9 to disable debug level logging
    public $loglevel = 2;

    public $profile = array(
        'default' => array(
            'path' => '/backuplog/logs/simplepay/default',
            'type' => 'file',
            'filename' => 'default'
        ),
		'cmp_processor' => array(
            'path' => '/backuplog/logs/simplepay/cmp_processor',
            'type' => 'file',
            'filename' => 'cmp_processor'
        ),
		'bniva_api' => array(
            'path' => '/backuplog/logs/simplepay/bniva/api',
            'type' => 'file',
            'filename' => 'api'
        ),
		'bniva_order_processor' => array(
            'path' => '/backuplog/logs/simplepay/bniva/order_processor',
            'type' => 'file',
            'filename' => 'order_processor'
        ),
        'bniva_order_completed_receiver' => array(
            'path' => '/backuplog/logs/simplepay/bniva/order_completed_receiver',
            'type' => 'file',
            'filename' => 'order_completed_receiver'
        ),
		'redison_api' => array(
            'path' => '/backuplog/logs/simplepay/redison/api',
            'type' => 'file',
            'filename' => 'api'
        ),
		'redison_order_processor' => array(
            'path' => '/backuplog/logs/simplepay/redison/order_processor',
            'type' => 'file',
            'filename' => 'order_processor'
        ),
        'redison_order_completed_receiver' => array(
            'path' => '/backuplog/logs/simplepay/redison/order_completed_receiver',
            'type' => 'file',
            'filename' => 'order_completed_receiver'
        ),
		'achiko_api' => array(
            'path' => '/backuplog/logs/simplepay/achiko/api',
            'type' => 'file',
            'filename' => 'api'
        ),
		'achiko_order_processor' => array(
            'path' => '/backuplog/logs/simplepay/achiko/order_processor',
            'type' => 'file',
            'filename' => 'order_processor'
        ),
        'achiko_order_completed_receiver' => array(
            'path' => '/backuplog/logs/simplepay/achiko/order_completed_receiver',
            'type' => 'file',
            'filename' => 'order_completed_receiver'
        ),
		'midtrans_api' => array(
            'path' => '/backuplog/logs/simplepay/midtrans/api',
            'type' => 'file',
            'filename' => 'api'
        ),
		'midtrans_order_processor' => array(
            'path' => '/backuplog/logs/simplepay/midtrans/order_processor',
            'type' => 'file',
            'filename' => 'order_processor'
        ),
        'midtrans_order_completed_receiver' => array(
            'path' => '/backuplog/logs/simplepay/midtrans/order_completed_receiver',
            'type' => 'file',
            'filename' => 'order_completed_receiver'
        ),
		'nicepay_api' => array(
            'path' => '/backuplog/logs/simplepay/nicepay/api',
            'type' => 'file',
            'filename' => 'api'
        ),
		'nicepay_order_processor' => array(
            'path' => '/backuplog/logs/simplepay/nicepay/order_processor',
            'type' => 'file',
            'filename' => 'order_processor'
        ),
        'nicepay_order_completed_receiver' => array(
            'path' => '/backuplog/logs/simplepay/nicepay/order_completed_receiver',
            'type' => 'file',
            'filename' => 'order_completed_receiver'
        ),
    );

}

<?php

class config_bniva {

	// PAYMENT GATEWAY CREDENTIALS
	public $payment_gateway = "BNIVA";
	
	// 3rd Party Path
	public $_3rdpartyPath = "/app/xmp2012/interface/simplepay/3rdparty/bniva";
	
	//dev
	public $prefix = "988";
	//prod
	//public $prefix = "988";
	
	//dev
	//public $clientID = "01575";
	//prod
	public $clientID = "10439";
	
	//dev
	//public $secretKey = "10762709aa31fe2ad821240e3ea97070";
	//prod
	public $secretKey = "a33de0159f9a94857b2d46ffe0d182c6";
	
	//dev
	//public $requestPaymentURL = "https://apibeta.bni-ecollection.com/";
	//prod
	public $requestPaymentURL = "https://api.bni-ecollection.com/";

	//public $type = "createbilling";
	//public $billing_type = "c";
	//public $requestPaymentURLSimulation = "http://dev.bni-ecollection.com/";
	
	// BUFFER REQUEST
	//public $bufferPath = '/app/xmp2012/buffers/simplepay/bniva';
	public $bufferSlot = 10;
	public $bufferThrottle = 500;
}

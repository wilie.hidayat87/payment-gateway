<?php

class config_midtrans {
	
	// PAYMENT GATEWAY CREDENTIALS
	public $payment_gateway = "MIDTRANS";
	
	//public $env = "development";
	public $env = "production";
	public $_3rdpartyPath = "/app/xmp2012/interface/simplepay/3rdparty/midtrans";
	
	//SANDBOX KEY
	/* public $serverKey = "SB-Mid-server-CfHHHMYw-PskBBgh-tStr_0t";
	public $clientKey = "SB-Mid-client-wkjdcdYKWefoGeBD";
	public $merchantId = "G177000576";
	public $snapJSURL = "https://app.sandbox.midtrans.com/snap/snap.js";
	public $paymentPage = "https://app.sandbox.midtrans.com/snap/v2/vtweb"; */
	
	//PRODUCTION KEY
	
	public $serverKey = "Mid-server-n_jqe6-Ga7mYoWpUoyDpUoAu";
	public $clientKey = "Mid-client-NI2qLHklkWIYZlbB";
	public $merchantId = "G177000576";
	public $snapJSURL = "https://app.midtrans.com/snap/snap.js";
	public $paymentPage = "https://app.midtrans.com/snap/v2/vtweb";
	
	
	public $mapNotificationDesc = array(
		 "capture_cc" 	=> "Transaction order_id: @order_id@ is challenged by FDS" // FOR CHALLANGE STATUS
		,"capture"		=> "Transaction order_id: @order_id@ successfully captured using @type@"
		,"settlement"	=> "Transaction order_id: @order_id@ successfully transfered using @type@"
		,"pending"		=> "Waiting customer to finish transaction order_id: @order_id@ successfully transfered using @type@"
		,"deny"			=> "Payment using @type@ for transaction order_id: @order_id@ is denied."
		,"expire"		=> "Payment using @type@ for transaction order_id: @order_id@ is expired."
		,"cancel"		=> "Payment using @type@ for transaction order_id: @order_id@ is canceled."
		,"challange"	=> "Transaction challange order_id: @order_id@ using @type@."
		,"error"		=> "Something wrong with the transaction order_id: @order_id@ using @type@, status code @status_code@."
	);
}
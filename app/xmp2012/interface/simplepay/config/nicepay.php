<?php

class config_nicepay {
	
	// PAYMENT GATEWAY CREDENTIALS
	public $payment_gateway = "NICEPAY";
	
	//public $env = "development";
	public $env = "production";

	//SANDBOX KEY
	public $merchantID_d = "OVOTEST001";
	//public $merchantID_d = "IONPAYTEST";
	public $merchantKey_d = "33F49GnCMS1mFYlGXisbUDzVf2ATWCl9k3R++d5hDd3Frmuos/XLx8XhXpe+LDYAbpGKZYSwtlyyLOtS/8aD7A==";
	
	//PRODUCTION KEY
	public $merchantID_p = "SURATSAK1T";
	public $merchantKey_p = "wwAWQ2429x2Esj8XsE2o/F3RZnV0/DDmFXk3iU4duiEtgem38G70O7lZ0orkeg02ZlvI0swdGIny4Eg6M88YsA==";
	
	//public $reqURL = "https://dev.nicepay.co.id";
	//public $reqURL = "https://staging.nicepay.co.id";
	public $reqURL = "https://www.nicepay.co.id"; 
	
	public $payment_method = "05"; // E-WALLET
	public $payment_services = array("OVO","DANA","LINK");
	public $cart_data_goods_qty = array("DANA","LINK");
	public $payment_code = array
	(
		"OVO" => array
		(
			"status" => array
			( 
				 "0" => "Paid"
				,"1" => "Void"
				,"8" => "Fail"
				,"9" => "Init"
			),
			//"imid" => "OVOTEST001",
			"imid" => "SURATSAK1T",
			"mitra_code" => "OVOE",
		),
		"DANA" => array
		(
			"status" => array
			(
				 "0" => "Paid"
				,"1" => "Void"
				,"2" => "Refund"
				,"3" => "Unpaid"
				,"4" => "Processing"
				,"5" => "Expired"
				,"8" => "Fail"
				,"9" => "Init"
			),
			//"imid" => "IONPAYTEST",
			"imid" => "SURATSAK1T",
			"mitra_code" => "DANA",
		),
		"LINK" => array
		(
			"status" => array
			(
				 "0" => "Paid"
				,"1" => "Void"
				,"2" => "Refund"
				,"3" => "Unpaid"
				,"8" => "Fail"
				,"9" => "Init"
			),
			//"imid" => "IONPAYTEST",
			"imid" => "SURATSAK1T",
			"mitra_code" => "LINK",
		),
	);
	
	//public $notification_url = "http://kbgrps.com/nicepay/";
	public $notification_url = "https://kbgrps.com/nicepay/notif/";
}
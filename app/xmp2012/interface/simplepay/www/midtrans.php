<?php
function requestPost($req) {
	
	$start = time();
		
	$ch = @curl_init();
	@curl_setopt($ch, CURLOPT_URL, $req['url']);
	
	if($req['port'] > 0)
		@curl_setopt($ch, CURLOPT_PORT, $req['port']);
	
	if(!empty($req['login']))
	{
		@curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		@curl_setopt($ch, CURLOPT_USERPWD, $req['login']); 
	}
	
	@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $req['ssl']);
	@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	@curl_setopt($ch, CURLOPT_TIMEOUT, $req['timeout']);
	@curl_setopt($ch, CURLOPT_POST, 1);
	@curl_setopt($ch, CURLOPT_POSTFIELDS, $req['body']);
	@curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
	
	if(!empty($req['headers']))
		@curl_setopt($ch, CURLOPT_HTTPHEADER, $req['headers']);

	$output = @curl_exec($ch);
	$header_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$header_out = @curl_getinfo($ch, CURLINFO_HEADER_OUT);
	@curl_close($ch);

	$elapse = (int)time() - (int)$start;

	$resp = array();
	$resp['header_code'] = $header_code;
	$resp['output'] = $output;
	
	return $resp;
}

$req['body'] = '{"gross_amount":10000,"first_name":"Wilie","last_name":"Hidayat","email":"coba@midtrans.cb","phone":"085724907141"}';

$req['url'] = "http://103.77.79.13:8575/api/midtrans/orderPayment/";
$req['port'] = 0;
$req['login'] = "";
$req['ssl'] = false;
$req['timeout'] = 5;

$req['headers'] = array(
	 'APPNAME: COBA',
	 'Content-type: application/json'
);

$resp = requestPost($req);

$output = json_decode($resp['output']);
//print_r($output->token);die;
$snapToken = $output->token;
$clientKey = $output->clientKey;
$snapJSURL = $output->snapJSURL;
//echo "snapToken = ".$snapToken;
//die;
?>

<!DOCTYPE html>
<html>
    <body>
        <script src="<?=$snapJSURL?>" data-client-key="<?=$clientKey?>"></script>
        <script type="text/javascript">
            //document.getElementById('pay-button').onclick = function(){
                // SnapToken acquired from previous step
                snap.pay('<?php echo $snapToken?>', {
                    // Optional
                    onSuccess: function(result){
                        /* You may add your own js here, this is just example */ document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
                    },
                    // Optional
                    onPending: function(result){
                        /* You may add your own js here, this is just example */ document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
                    },
                    // Optional
                    onError: function(result){
                        /* You may add your own js here, this is just example */ document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
                    }
                });
            //};
        </script>
    </body>
</html>
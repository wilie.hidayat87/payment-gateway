<?php
class simplepay_invmodel_cmp
{
    public static function putPixel($p)
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start : ' . serialize($p)));
		
		// Load a model and create a connection
		
		$q = loader_model::getInstance()->load('bridge', 'trx');

		$SQL = sprintf("INSERT INTO cmp.pixel_storage VALUE (DEFAULT, '%s', '%s', '%s', NOW(), 0, '%s', '%s', '%s')", 
			 $q->filter($p['pixel'])
			,$q->filter($p['operator'])
			,$q->filter($p['service'])
			,$q->filter($p['partner'])
			,$q->filter($p['campurl'])
			,$q->filter($p['msisdn'])
		);
		
		$lId = $q->eSql($SQL);
		
		// Close a connection
			
		$q->closeConnection();
			
		return $lId;
	}
}
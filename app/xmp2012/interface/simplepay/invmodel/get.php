<?php
class simplepay_invmodel_get
{
    public static function getListPaymentChannel()
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start'));
		
		// Load a model and create a connection
		$configDB = loader_config::getInstance()->getConfig('database');
		
		$q = loader_model::getInstance()->load('bridge', 'trx');
					
		$rows = $q->qSql("SELECT d.id AS denomid, d.channelid AS channelid, pc.channel, d.currency, d.amount, d.amount_desc FROM {$configDB->profile['trx']['database']}.payment_channel pc INNER JOIN {$configDB->profile['trx']['database']}.denoms d ON pc.id = d.channelid ORDER BY d.channelid, d.amount ASC;");
		
		// Close a connection
					
		$q->closeConnection();
		
		return $rows;
	}
	
	public static function getTransaction($p)
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start' . serialize($p)));
		
		// Load a model and create a connection
		$configDB = loader_config::getInstance()->getConfig('database');
		
		$q = loader_model::getInstance()->load('bridge', 'trx');
		
		$userid = $q->filter($p['userid']);
		$payment_channel = $q->filter($p['payment_channel']);
		$currency = $q->filter($p['currency']);
		$amount = $q->filter($p['amount']);
		$item_id = $q->filter($p['item_id']);
		$item_name = $q->filter($p['item_name']);
		$merchant_transid = $q->filter($p['merchant_transid']);
			
		$extSQL = "";
		
		if(!empty($userid))
			$extSQL .= sprintf(" AND userid = '%s'", $userid);
		if(!empty($payment_channel))
			$extSQL .= sprintf(" AND payment_channel = '%s'", $payment_channel);
		if(!empty($currency))
			$extSQL .= sprintf(" AND currency = '%s'", $currency);
		if(!empty($amount))
			$extSQL .= sprintf(" AND amount = '%s'", $amount);
		if(!empty($item_id))
			$extSQL .= sprintf(" AND item_id = %d", $item_id);
		if(!empty($item_name))
			$extSQL .= sprintf(" AND item_name = '%s'", $item_name);
		
		$SQL = sprintf("SELECT * FROM {$configDB->profile['trx']['database']}.transaction WHERE 1=1 AND merchant_transid = '%s' {$extSQL} LIMIT 1;", 
			 $q->filter($p['merchant_transid'])
		);
		
		$rows = $q->qSql($SQL);
		
		// Close a connection
					
		//$q->closeConnection();
		
		return $rows;
	}
	
	public static function getSubs($p)
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start' . serialize($p)));
		
		// Load a model and create a connection
		$configDB = loader_config::getInstance()->getConfig('database');
		
		$q = loader_model::getInstance()->load('bridge', 'trx');
		
		$SQL = sprintf("SELECT * FROM {$configDB->profile['trx']['database']}.subscription WHERE app_name = '%s' AND userid = '%s' LIMIT 1;", 
			 $q->filter($p['app_name'])
			,$q->filter($p['userid'])
		);
		
		//echo $SQL;die;
		$rows = $q->qSql($SQL);
		
		// Close a connection
					
		//$q->closeConnection();
		
		return $rows;
	}
	
	public static function getBufferPayment($p)
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start' . serialize($p)));
		
		// Load a model and create a connection
		$configDB = loader_config::getInstance()->getConfig('database');
		
		$q = loader_model::getInstance()->load('bridge', 'trx');
		
		$SQL = sprintf("SELECT * FROM {$configDB->profile['trx']['database']}.buffer_payment WHERE time_expired < NOW() AND status = 0 AND slot = %d LIMIT %d;", 
			$q->filter($p['slot'])
		   ,$q->filter($p['limit'])
		);
		
		//echo $SQL;die;
		$rows = $q->qSql($SQL);
		
		return $rows;
	}
}
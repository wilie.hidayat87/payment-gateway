<?php
class simplepay_invmodel_set
{
    public static function setTransaction($p)
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start : ' . serialize($p)));
		
		// Load a model and create a connection
		$configDB = loader_config::getInstance()->getConfig('database');

		$q = loader_model::getInstance()->load('bridge', 'trx');

		$SQL = sprintf("INSERT INTO {$configDB->profile['trx']['database']}.transaction VALUE (DEFAULT, '%s', '%s', '%s', '%s', '%s', '%s', %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", 
			 $q->filter($p['payment_gateway'])
			,$q->filter($p['app_name'])
			,$q->filter($p['msisdn'])
			,$q->filter($p['payment_channel'])
			,$q->filter($p['currency'])
			,$q->filter($p['amount'])
			,$q->filter($p['item_id'])
			,$q->filter($p['item_name'])
			,$q->filter($p['time_request_order'])
			,$q->filter($p['time_response_order'])
			,$q->filter($p['time_confirmed_order'])
			,$q->filter($p['status_code'])
			,$q->filter($p['reason_code'])
			,$q->filter($p['merchant_transid'])
			,$q->filter($p['transact_desc'])
			,$q->filter($p['redirect_url'])
			,$q->filter($p['confirm_page'])
			,$q->filter($p['custom'])
			,$q->filter($p['reserve1'])
			,$q->filter($p['reserve2'])
			,$q->filter($p['reserve3'])
		);
		
		$lId = $q->eSql($SQL);
		
		// Close a connection
		
		if($p['closing_connection'] === false){
			// do nothing
		}
		else{
			$q->closeConnection();
		}
			
		return $lId;
	}
	
	public static function updateTransaction($p)
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start : ' . serialize($p)));
		
		// Load a model and create a connection
		$configDB = loader_config::getInstance()->getConfig('database');
		
		$q = loader_model::getInstance()->load('bridge', 'trx');

		$time_confirmed_order = $q->filter($p['time_confirmed_order']);
		$status_code = $q->filter($p['status_code']);
		$reason_code = $q->filter($p['reason_code']);
		$app_name = $q->filter($p['app_name']);
		$userid = $q->filter($p['userid']);
		$payment_channel = $q->filter($p['payment_channel']);
		$transact_desc = $q->filter($p['transact_desc']);
		$currency = $q->filter($p['currency']);
		$amount = $q->filter($p['amount']);
		$item_id = $q->filter($p['item_id']);
		$item_name = $q->filter($p['item_name']);
		$merchant_transid = $q->filter($p['merchant_transid']);
		$payment_gateway = $q->filter($p['payment_gateway']);
		$reserve1 = $q->filter($p['reserve1']);
		$reserve2 = $q->filter($p['reserve2']);
		$reserve3 = $q->filter($p['reserve3']);
		
		$set = "";
		$set .= sprintf("time_confirmed_order = '%s', ", $time_confirmed_order);
		$set .= sprintf("status_code = '%s', ", $status_code);
		$set .= sprintf("reason_code = '%s', ", $reason_code);
		$set .= sprintf("payment_channel = '%s', ", $payment_channel);
		$set .= sprintf("transact_desc = '%s', ", $transact_desc);
		$set .= sprintf("reserve1 = '%s', ", $reserve1);
		$set .= sprintf("reserve2 = '%s', ", $reserve2);
		$set .= sprintf("reserve3 = '%s'", $reserve3);
		
		$where_clause = "";
		
		$where_clause .= sprintf(" AND app_name = '%s'", $app_name);
		$where_clause .= sprintf(" AND userid = '%s'", $userid);
		$where_clause .= sprintf(" AND currency = '%s'", $currency);
		$where_clause .= sprintf(" AND amount = '%s'", $amount);
		$where_clause .= sprintf(" AND item_id = %d", $item_id);
		$where_clause .= sprintf(" AND item_name = '%s'", $item_name);
		
		$SQL = sprintf("UPDATE {$configDB->profile['trx']['database']}.transaction SET {$set} WHERE 1=1 AND merchant_transid = '%s' AND payment_gateway = '%s' {$where_clause}", 
			 $q->filter($p['merchant_transid'])
			,$q->filter($p['payment_gateway'])
		);
		
		//echo $SQL;die;
		$q->eSql($SQL);
		
		// Close a connection
			
		return true;
	}
	
	public static function insertSubs($p)
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start : ' . serialize($p)));
		
		// Load a model and create a connection
		$configDB = loader_config::getInstance()->getConfig('database');
		
		$q = loader_model::getInstance()->load('bridge', 'trx');

		$SQL = sprintf("INSERT INTO {$configDB->profile['trx']['database']}.subscription VALUE (DEFAULT, %d, '%s', '%s', '%s', '%s', %d, %d, NOW(), NOW())", 
			 $q->filter($p['userid'])
			,$q->filter($p['payment_gateway'])
			,$q->filter($p['payment_channel'])
			,$q->filter($p['app_name'])
			,$q->filter($p['currency'])
			,$q->filter($p['amount'])
			,$q->filter($p['active'])
		);
		
		$lId = $q->eSql($SQL);
		
		$q->closeConnection();
		
		return $lId;
	}
	
	public static function updateSubs($p)
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start : ' . serialize($p)));
		
		// Load a model and create a connection
		$configDB = loader_config::getInstance()->getConfig('database');
		
		$q = loader_model::getInstance()->load('bridge', 'trx');

		$SQL = sprintf("UPDATE {$configDB->profile['trx']['database']}.subscription SET payment_channel = '%s', currency = '%s', amount = %d, active = %d, latest_charging = NOW() WHERE userid = %d AND app_name = '%s' AND payment_gateway = '%s'", 
			 $q->filter($p['payment_channel'])
			,$q->filter($p['currency'])
			,$q->filter($p['amount'])
			,$q->filter($p['active'])
			,$q->filter($p['userid'])
			,$q->filter($p['app_name'])
			,$q->filter($p['payment_gateway'])
		);
		
		//echo $SQL;die;
		$lId = $q->eSql($SQL);
		
		$q->closeConnection();
		
		return $lId;
	}
	
	public static function bufferPayment($p)
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start : ' . serialize($p)));
		
		// Load a model and create a connection
		$configDB = loader_config::getInstance()->getConfig('database');
		
		$q = loader_model::getInstance()->load('bridge', 'trx');

		$SQL = sprintf("INSERT INTO {$configDB->profile['trx']['database']}.buffer_payment VALUE (DEFAULT, '%s', '%s', %d, %d, %d, NOW(), NOW(), '%s')", 
			 $q->filter($p['payment_gateway'])
			,$q->filter($p['app_name'])
			,$q->filter($p['status'])
			,$q->filter($p['priority'])
			,$q->filter($p['slot'])
			,$q->filter($p['obj'])
		);
		
		$lId = $q->eSql($SQL);
		
		$q->closeConnection();
		
		return $lId;
	}
	
	public static function updatebufferPayment($p)
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start : ' . serialize($p)));
		
		// Load a model and create a connection
		$configDB = loader_config::getInstance()->getConfig('database');
		
		$q = loader_model::getInstance()->load('bridge', 'trx');

		$SQL = sprintf("UPDATE {$configDB->profile['trx']['database']}.buffer_payment SET status = %d WHERE id = %d", 
			 $q->filter($p['status'])
			,$q->filter($p['id'])
		);
		
		$q->eSql($SQL);
		
		return true;
	}
	
	public static function closeConn()
	{
		// Load a model and create a connection
		$configDB = loader_config::getInstance()->getConfig('database');
		
		$q = loader_model::getInstance()->load('bridge', 'trx');

		$q->closeConnection();
		
		return true;
	}
}
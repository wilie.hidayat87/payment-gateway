<?php
class simplepay_controller_achiko_ReceivePaymentConfirmation
{
	public static function dr()
	{
		$bodyRequest = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		
		$log_profile = 'achiko_order_completed_receiver';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => 'Start : ' . $bodyRequest));
		
		//5e945f3e3a48d2681       info    2020-04-13 19:46:54     0.238811        simplepay_dr_notification       notificationReceiver    Start : s:475:"{"data":{"id":"5e945ec677b09b16bf3f132e","timestamp":1586782014,"details":{"app_id":"5e8dede477b09b3d6542ff58","user_id":"088881234567","merchant_transaction_id":"2020041319445415867818945135","transaction_description":"payment attempt","payment_channel":"airtime_testing","channel_name":"Airtime Testing","currency":"IDR","amount":50000,"status_code":"PAYMENT_COMPLETED","status":"Payment Completed","item_id":1,"item_name":"ORDER_REQUIREMENT_1","testing":"1","custom":""}}}";

		$configAchiko = loader_config::getInstance()->getConfig('achiko');
		
		$checkJsonParams = http_validate::jsonValidate($bodyRequest);
		$parameters = $checkJsonParams['result'];
		
		// Get Transaction by Merchant ID
		
		$rows = simplepay_invmodel_get::getTransaction(array(
			 'userid' 				=> $parameters->data->details->user_id
			,'payment_gateway' 		=> $configAchiko->payment_gateway
			,'payment_channel' 		=> $parameters->data->details->payment_channel
			,'currency' 			=> $parameters->data->details->currency
			,'amount' 				=> $parameters->data->details->amount
			,'item_id' 				=> $parameters->data->details->item_id
			,'item_name' 			=> $parameters->data->details->item_name
			,'merchant_transid' 	=> $parameters->data->id
		));
		
		// Save transaction Order Payment Confirmation in the database
			
		simplepay_invmodel_set::updateTransaction(array(
			 'payment_gateway' 		=> $configAchiko->payment_gateway
			,'app_name' 			=> $rows[0]['app_name']
			,'userid' 				=> $parameters->data->details->user_id
			,'payment_channel' 		=> $parameters->data->details->payment_channel
			,'currency' 			=> $parameters->data->details->currency
			,'amount' 				=> $parameters->data->details->amount
			,'item_id' 				=> $parameters->data->details->item_id
			,'item_name' 			=> $parameters->data->details->item_name
			,'time_confirmed_order' => date("Y-m-d H:i:s", $parameters->data->timestamp)
			,'status_code' 			=> $parameters->data->details->status_code
			,'reason_code' 			=> $parameters->data->details->status
			,'merchant_transid' 	=> $parameters->data->id
		));
		
		// Get Subscription data by user id & app name
		// - Adding subscription method when receive notification payment
		
		$active = 0;
		if($parameters->data->details->status_code == "PAYMENT_COMPLETED")
			$active = 1;
		
		$subs = simplepay_invmodel_get::getSubs(array(
			 'app_name' => $rows[0]['app_name']
			,'userid' 	=> $parameters->data->details->user_id
		));
		
		//print_r($subs);die;
		if(count($subs) > 0){
			
			// Update Subscription data when subs is not empty
				
			simplepay_invmodel_set::updateSubs(array(
				 'payment_gateway' 			=> $configAchiko->payment_gateway
				,'app_name' 				=> $rows[0]['app_name']
				,'userid' 					=> $parameters->data->details->user_id
				,'payment_channel' 			=> $parameters->data->details->payment_channel
				,'currency' 				=> $parameters->data->details->currency
				,'amount' 					=> $parameters->data->details->amount
				,'active' 					=> $active
			));
			
		}else{
			
			// Otherwise insert into subscription table when is empty
			
			simplepay_invmodel_set::insertSubs(array(
				 'payment_gateway' 			=> $configAchiko->payment_gateway
				,'app_name' 				=> $rows[0]['app_name']
				,'userid' 					=> $parameters->data->details->user_id
				,'payment_channel' 			=> $parameters->data->details->payment_channel
				,'currency' 				=> $parameters->data->details->currency
				,'amount' 					=> $parameters->data->details->amount
				,'active' 					=> $active
			));
		}
		
		// Filter by app name for requirement project
		
		switch($rows[0]['app_name'])
		{
			case 'SPARSA' :
				
				$req['body'] = implode("&", array(
					 'trx_id=' 		. $parameters->data->id
					,'currency=' 	. $parameters->data->details->currency
					,'amount='		. $parameters->data->details->amount
					,'msisdn='		. $parameters->data->details->user_id
					,'status='		. $parameters->data->details->status_code
					,'datetime='	. date("YmdHis", $parameters->data->timestamp)
				));

				$req['url'] = "http://103.77.79.131/sparsa/portal/action/dr-payment.php?";
				$req['port'] = 0;
				$req['login'] = "";
				$req['ssl'] = false;
				$req['timeout'] = 5;
				$req['headers'] = array();
				
				http_request::requestGet($req, "Order Payment");
				
			default : 
				
			break;
		}
		
		return $result;
	}
}
<?php
class simplepay_controller_achiko_OrderChecking
{
	public static function check()
	{
		$bodyRequest = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start : ' . $bodyRequest));
		
		$checkJsonParams = http_validate::jsonValidate($bodyRequest);
		
		$log->write(array('level' => 'info', 'message' => 'Request Checker : ' . print_r($checkJsonParams,1)));
		
		if($checkJsonParams['error'] === FALSE)
		{					
			$params = $checkJsonParams['result'];
			
			$id = (int)trim($params->id);
			$timestamp = trim($params->timestamp);
			
			/* $id = http_validate::valChecker($params->id, "string");
			$timestamp = http_validate::valChecker($params->timestamp, "int"); */
			
			if(empty($id))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : id not found!'));
			}
			else if(empty($timestamp))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : timestamp not found!'));
			}
			else
			{
				$requestHit = simplepay_module_achiko::orderChecking(array(
					 "id"			=> trim($id)
					,"timestamp"	=> trim($timestamp)
				));
				
				http_response_code((int)$requestHit['header_code']);
				
				return $requestHit['output'];
			}
		}
		else
		{
			$log->write(array('level' => 'info', 'message' => 'Start'));
		
			http_response_code(400);
			
			$result = json_encode(array('err' => 'Bad Request, check your parameters'));
		}
						
		return $result;
	}
}
<?php
class simplepay_controller_midtrans_Notification
{
	public static function fetch()
	{	
		$log_profile = 'midtrans_order_completed_receiver';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
		
		$midtransConf = loader_config::getInstance()->getConfig('midtrans');
		
		require_once $midtransConf->_3rdpartyPath . '/Veritrans.php';
		
		// Uncomment for production environment
		if($midtransConf->env == "development")
			Veritrans_Config::$isProduction = false;
		else
			Veritrans_Config::$isProduction = true;
		
		Veritrans_Config::$serverKey = $midtransConf->serverKey;
		
        try 
		{
			$bodyRequest = new Veritrans_Notification();
		  
			$log->write(array('level' => 'info', 'message' => 'Veritrans Notification : ' . print_r($bodyRequest,1)));
		  
			$requestHit = simplepay_module_midtrans::notification($bodyRequest);
				
			http_response_code(200);
			
		} catch (Exception $e) {
		  
		  $bodyRequest = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		  
		  $log->write(array('level' => 'info', 'message' => "Exception: ".$e->getMessage().", Notification received: ".$bodyRequest));
		} 		
		
		return true;
	}
}
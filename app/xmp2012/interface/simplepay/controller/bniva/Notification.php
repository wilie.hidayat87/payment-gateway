<?php
class simplepay_controller_bniva_Notification
{
	public static function dr()
	{
		$bodyRequest = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		
		$log_profile = 'bniva_order_completed_receiver';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => 'NOTIF BNIVA : ' . $bodyRequest));
		
		/*
		{
			 "client_id":"001"
			,"data":ENCRYPT DATA
		}
		
		{
			 "client_id":"001"
			,"data":{
				 "trxid":"1234153498346"
				,"virtual_account":"1235135305345"
				,"customer_name":"Wilie"
				,"trx_amount":10000
				,"payment_amount":10000
				,"cumulative_payment_amount":10000
				,"payment_ntb":"233171"
				,"datetime_payment":"2020-06-19 16:37:59"
				,"datetime_payment_iso8601":"2020-06-19T16:37:59+07:00"
			}
		}
		
		
		*/
		
		$bnivaConf = loader_config::getInstance()->getConfig('bniva');

		require_once $bnivaConf->_3rdpartyPath . '/BniEnc.php';
		
		$output = json_decode($bodyRequest, true);
		
		$data_response = BniEnc::decrypt($output['data'], $bnivaConf->clientID, $bnivaConf->secretKey);
		
		$log->write(array('level' => 'info', 'message' => 'Notification Data - ' . print_r($data_response,1)));
		
		// Inquiry check existing billing
		
		$inquiry = simplepay_module_bniva::inquiryBilling(array("trx_id" => $data_response['trx_id']));
		
		$log->write(array('level' => 'info', 'message' => 'Inquiry Data - ' . serialize($inquiry)));
		
		if(!empty($inquiry))
		{
			if($inquiry['status'] == '000')
			{
				// Get Transaction by Merchant ID
				
				$rows = simplepay_invmodel_get::getTransaction(array(
					 'payment_gateway' 		=> $bnivaConf->payment_gateway
					,'amount' 				=> $inquiry['data']['trx_amount']
					,'merchant_transid' 	=> $inquiry['data']['trx_id']
				));
				
				if(count($rows) > 0)
				{
					$data = array(
						 'amount' 				=> (isset($inquiry['data']['trx_amount']) ? $inquiry['data']['trx_amount'] : $rows[0]['amount'])
						,'trx_id'				=> (isset($inquiry['data']['trx_id']) ? $inquiry['data']['trx_id'] : $rows[0]['merchant_transid'])
						,'time_confirmed_order'	=> (isset($inquiry['data']['datetime_payment']) ? $inquiry['data']['datetime_payment'] : date("Y-m-d H:i:s"))
						,'app_name'				=> (isset($rows[0]['app_name']) ? $rows[0]['app_name'] : "")
						,'userid'				=> (isset($rows[0]['userid']) ? $rows[0]['userid'] : "")
						,'currency'				=> (isset($rows[0]['currency']) ? $rows[0]['currency'] : "IDR")
						,'item_id'				=> (isset($rows[0]['item_id']) ? $rows[0]['item_id'] : "")
						,'item_name'			=> (isset($rows[0]['item_name']) ? $rows[0]['item_name'] : "") 
						,'status_code'			=> $inquiry['status']
						,'reason_code'			=> $inquiry['status']
						,'transact_desc'		=> (isset($rows[0]['transact_desc']) ? $rows[0]['transact_desc'] : "")
						,'reserve1'				=> (isset($rows[0]['reserve1']) ? $rows[0]['reserve1'] : "")
						,'reserve2'				=> (isset($rows[0]['reserve2']) ? $rows[0]['reserve2'] : "")
						,'reserve3'				=> (isset($rows[0]['reserve3']) ? $rows[0]['reserve3'] : "")
					);
					
					// Save transaction Order Payment Confirmation in the database
						
					simplepay_invmodel_set::updateTransaction(array(
						 'payment_gateway' 		=> $bnivaConf->payment_gateway
						,'app_name' 			=> $data['app_name']
						,'userid' 				=> $data['userid']
						,'payment_channel' 		=> $bnivaConf->payment_gateway
						,'currency' 			=> $data['currency']
						,'amount' 				=> $data['amount']
						,'item_id' 				=> $data['item_id']
						,'item_name' 			=> $data['item_name']
						,'merchant_transid' 	=> $data['trx_id']
						,'time_confirmed_order' => $data['time_confirmed_order']
						,'status_code' 			=> $data['status_code']
						,'reason_code' 			=> $data['reason_code']
						,'transact_desc' 		=> $data['transact_desc']
						,'reserve1' 			=> $data['reserve1']
						,'reserve2' 			=> $data['reserve2']
						,'reserve3' 			=> $data['reserve3']
					));
					
					// Get Subscription data by user id & app name
					// - Adding subscription method when receive notification payment
					
					$subs = simplepay_invmodel_get::getSubs(array(
						 'app_name' => $data['app_name']
						,'userid' 	=> $data['userid']
					));
					
					//print_r($subs);die;
					if(count($subs) > 0){
						
						// Update Subscription data when subs is not empty
							
						simplepay_invmodel_set::updateSubs(array(
							 'payment_gateway' 			=> $bnivaConf->payment_gateway
							,'app_name' 				=> $data['app_name']
							,'userid' 					=> $data['userid']
							,'payment_channel' 			=> $bnivaConf->payment_gateway
							,'currency' 				=> $data['currency']
							,'amount' 					=> $data['amount']
							,'active' 					=> 1
						));
						
					}else{
						
						// Otherwise insert into subscription table when is empty
						
						simplepay_invmodel_set::insertSubs(array(
							 'payment_gateway' 			=> $bnivaConf->payment_gateway
							,'app_name' 				=> $data['app_name']
							,'userid' 					=> $data['userid']
							,'payment_channel' 			=> $bnivaConf->payment_gateway
							,'currency' 				=> $data['currency']
							,'amount' 					=> $data['amount']
							,'active' 					=> 1
						));
					}
					
					// Filter by app name for requirement project
					
					switch($rows[0]['app_name'])
					{
						case 'SURATSAKIT' :
							
							$req = array();
							
							$req['body'] = json_encode(array(
								 'trx_id' 	=> $data['trx_id']
								,'status'	=> $data['status_code']
								,'reason' 	=> $data['reason_code']
								,'datetime' => str_replace(" ","",str_replace("-","",str_replace(":","",$data['trx_id'])))
							));

							$req['url'] = "https://suratsakit.com/action/dr-payment-va-api.php";
							$req['port'] = 0;
							$req['login'] = "";
							$req['ssl'] = false;
							$req['timeout'] = 5;
							$req['headers'] = array(
								'Content-Type: application/json',
								//'Accept-Encoding: gzip, deflate',
								'Cache-Control: max-age=0',
								'Connection: keep-alive',
								'Accept-Language: en-US,en;q=0.8,id;q=0.6',
								'APPNAME: ' . $data['app_name'],
								'AUTH_USER: ' . $data['app_name'],
								'AUTH_PWD: '.md5('suratsakitPass2020'),
							);
							
							http_request::requestPost($req, "NOTIF SURATSAKIT");
						
						break;
						
						case 'SPARSA' :
							
							$req = array();
							
							$req['body'] = json_encode(array(
								 'trx_id' 	=> $data['trx_id']
								,'status'	=> $data['status_code']
								,'reason' 	=> $data['reason_code']
								,'datetime' => str_replace(" ","",str_replace("-","",str_replace(":","",$data['trx_id'])))
							));

							$req['url'] = "http://sparsa.id/mobile/action/dr-payment-api.php";
							$req['port'] = 0;
							$req['login'] = "";
							$req['ssl'] = false;
							$req['timeout'] = 5;
							$req['headers'] = array(
								'Content-Type: application/json',
								//'Accept-Encoding: gzip, deflate',
								'Cache-Control: max-age=0',
								'Connection: keep-alive',
								'Accept-Language: en-US,en;q=0.8,id;q=0.6',
								'APPNAME: ' . $data['app_name'],
								'AUTH_USER: ' . $data['app_name'],
								'AUTH_PWD: '.md5('SparsaPass2020'),
							);
							
							http_request::requestPost($req, "NOTIF SPARSA");
						
						break;
						
						case 'RAPIDTEST' :
							
							$req = array();
							
							$req['body'] = json_encode(array(
								 'trx_id' 	=> $data['trx_id']
								,'status'	=> $data['status_code']
								,'reason' 	=> $data['reason_code']
								,'datetime' => str_replace(" ","",str_replace("-","",str_replace(":","",$data['trx_id'])))
							));

							$req['url'] = "http://cepatsehat.net/rapid/action/dr-payment-va-api.php";
							$req['port'] = 0;
							$req['login'] = "";
							$req['ssl'] = false;
							$req['timeout'] = 5;	
							$req['headers'] = array(
								'Content-Type: application/json',
								//'Accept-Encoding: gzip, deflate',
								'Cache-Control: max-age=0',
								'Connection: keep-alive',
								'Accept-Language: en-US,en;q=0.8,id;q=0.6',
								'APPNAME: ' . $data['app_name'],
								'AUTH_USER: ' . $data['app_name'],
								'AUTH_PWD: '.md5('suratsakitPass2020'),
							);
							
							http_request::requestPost($req, "NOTIF RAPIDTEST");
						
						break;
						
						case 'SURATCOVID' :
							
							$req = array();
							
							$req['body'] = json_encode(array(
								 'trx_id' 	=> $data['trx_id']
								,'status'	=> $data['status_code']
								,'reason' 	=> $data['reason_code']
								,'datetime' => str_replace(" ","",str_replace("-","",str_replace(":","",$data['trx_id'])))
							));

							$req['url'] = "http://suratcovid.com/app/index.php/va-bni-paynotif";
							$req['port'] = 0;
							$req['login'] = "";
							$req['ssl'] = false;
							$req['timeout'] = 5;	
							$req['headers'] = array(
								'Content-Type: application/json',
								//'Accept-Encoding: gzip, deflate',
								'Cache-Control: max-age=0',
								'Connection: keep-alive',
								'Accept-Language: en-US,en;q=0.8,id;q=0.6',
								'APPNAME: ' . $data['app_name'],
							);
							
							http_request::requestPost($req, "NOTIF SURATCOVID");
						
						break;
						
						default : 
							
						break;
					}
				}
			}
		}
		
		return array("status" => $inquiry['status']);
	}
}
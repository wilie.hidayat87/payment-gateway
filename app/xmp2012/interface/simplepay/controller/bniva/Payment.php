<?php
class simplepay_controller_bniva_Payment
{
	public static function order()
	{
		$bodyRequest = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		
		$log_profile = 'bniva_order_processor';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => 'Start : ' . $bodyRequest));
						
		$checkJsonParams = http_validate::jsonValidate($bodyRequest);
		
		$log->write(array('level' => 'info', 'message' => 'Request Checker : ' . print_r($checkJsonParams,1)));
		
		if($checkJsonParams['error'] === FALSE)
		{
			$params = $checkJsonParams['result'];
			
			$custname = trim($params->custname);
			$msisdn = (int)trim($params->msisdn);
			$email = trim($params->email);
			$currency = trim($params->currency);
			$amount = (int)trim($params->amount);
			
			if(empty($custname))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : custname not found!'));
			}
			else if(empty($msisdn))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : msisdn not found!'));
			}
			else if(empty($email))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : email not found!'));
			}
			else if(empty($currency))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : currency not found!'));
			}
			else if(empty($amount))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : amount not found!'));
			}
			else
			{				
				$bnivaConf = loader_config::getInstance()->getConfig('bniva');
				
				$datetime = date ( "Y-m-d H:i:s" );
				$app_name = trim($_SERVER['HTTP_APPNAME']);
				
				$original_data = array(
					 "trx_id"			=> str_replace(" ", "", str_replace(":", "", str_replace("-", "", $datetime))) . str_replace ( '.', '', microtime ( true ) )
					,"custname"			=> $custname
					,"msisdn"			=> $msisdn
					,"email"			=> $email
					,"currency"			=> $currency
					,"amount"			=> $amount
				);
				
				$obj = json_encode($original_data);
				
				$data = array(
					 'payment_gateway'		=> $bnivaConf->payment_gateway
					,'app_name' 			=> (isset($app_name) ? $app_name : 'COMPANY_CONFIDENTIAL')
					,'status' 				=> 0
					,'priority' 			=> 0
					,'slot' 				=> rand(0, $bnivaConf->bufferSlot-1)
					,'obj' 					=> $obj
				);
				
				$log->write(array('level' => 'info', 'message' => 'Buffer Data : ' . json_encode($data)));
				
				simplepay_invmodel_set::bufferPayment($data);
				
				$requestHit['header_code'] = 200;
				$requestHit['output'] = array("status" => "OK", 
					"data" => $original_data
				);
				
				http_response_code(200);
				
				// CPA CAMPAIGN CHECKER TO PIXEL STORAGE
				
				if((int)$requestHit['header_code'] == 200 && $params->custom == "campaign")
				{
					simplepay_controller_managePixel::buffer(array(
						"operator" 	=> $app_name."_".$currency."_".$amount, 
						"msisdn" 	=> $msisdn, 
						"service" 	=> $app_name
					));
				}
				
				return json_encode($requestHit);
			}
		}
		else
		{
			$log->write(array('level' => 'info', 'message' => 'Start'));
		
			http_response_code(400);
			
			$result = json_encode(array('err' => 'Bad Request, check your parameters'));
		}
						
		return $result;
	}
	
	public static function processPayment()
	{
		global $params;
		
		$slot = $params['t'];
		$limit = $params['n'];

		$lockPath = '/tmp/lock_bniva_processPayment_' . $slot . '_' . $limit;

		if (file_exists ( $lockPath )) {
			//$log->write ( array ('level' => 'debug', 'message' => "Lock File Exist on : " . $lockPath ) );
			echo "NOK - Lock File Exist on $lockPath \n";
			exit ( 0 );
		} else {
			touch ( $lockPath );
		}
		
		$buffers = simplepay_invmodel_get::getBufferPayment(array(
			 'slot' 	=> $slot
			,'limit' 	=> $limit
		));
		
		if(count($buffers) > 0)
		{
			$log_profile = 'bniva_order_processor';
			$log = manager_logging::getInstance();
			$log->setProfile($log_profile);
			$log->write ( array ('level' => 'debug', 'message' => "Slot [{$slot}], Limit [{$limit}]" ) );
		
			foreach ( $buffers as $rec ) 
			{
				//print_r($rec);
				simplepay_invmodel_set::updatebufferPayment(array("id" => $rec['id'], "status" => 1));
				
				$params = json_decode($rec['obj']);
				
				$requestHit = simplepay_module_bniva::createBilling(array(
					 "app_name"			=> $rec['app_name']
					,"trx_id"			=> $params->trx_id
					,"custname"			=> $params->custname
					,"msisdn"			=> $params->msisdn
					,"email"			=> $params->email
					,"currency"			=> $params->currency
					,"amount"			=> $params->amount
				));
			}
		}
		
		unlink ( $lockPath );
		
        return true;
    }
}
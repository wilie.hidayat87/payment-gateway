<?php
class simplepay_controller_managePixel
{
	public static function put()
	{
		//$bodyRequest = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		//$bodyRequest = $_REQUEST;
		
		$log_profile = 'cmp_processor';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => 'Start : ' . json_encode($_REQUEST)));
		
		$thePixel = strtolower(trim($_REQUEST['pixel']));
		$lengthPixel = strlen($thePixel);
		
		$operator = (!empty($_REQUEST['operator'])) ? trim($_REQUEST['operator']) : '';
		$service = (!empty($_REQUEST['servicename'])) ? trim($_REQUEST['servicename']) : '';
		$campurl = (!empty($_REQUEST['campurl'])) ? trim($_REQUEST['campurl']) : '';
		$msisdn = (!empty($_REQUEST['msisdn'])) ? trim($_REQUEST['msisdn']) : '';
		
		if(strpos($thePixel,"abcdxyz") !== FALSE) { $partner = 'testing_ad'; }
		else if($lengthPixel == 55) { $partner = 'kimia'; }
		else if($lengthPixel == 23) { $partner = 'mobusi'; }
		else if($lengthPixel == 45) { 
			
			if(substr($thePixel,0,2) == "c-")
				$partner = 'mcatch'; 
		}
		else if($lengthPixel == 24) 
		{
			/* if (ctype_upper($thePixel))
				$partner = 'adm'; 
			else
				$partner = 'cpark'; */ 
			
			if(substr($thePixel, 11, 6) == "103000")
				$partner = 'cpark'; 
			else if (substr($thePixel, 11, 6) == "242000")
				$partner = 'harrenmedia'; 
			else 
				$partner = 'adm';
		}
		else if($lengthPixel == 12) { $partner = 'elymob'; }
		else if($lengthPixel == 14) { $partner = 'mbp'; }
		else if($lengthPixel == 35) { $partner = 'revlink'; }
		else if($lengthPixel == 41) { $partner = 'collectcent'; }
		else if($lengthPixel == 62) { $partner = 'dlt'; }
		else if($lengthPixel == 40 || $lengthPixel == 41) { $partner = 'tfc'; }
		else if($lengthPixel == 18 || $lengthPixel == 30) 
		{ 
			if(substr($thePixel, 0, 2) == "01" || substr($thePixel, 0, 2) == "02") { $partner = 'olimob'; }
		}
		else if($lengthPixel == 36 || $lengthPixel == 39) 
		{ 
			$arrPixel = explode("-", $thePixel);
			
			if($arrPixel[0] == date("Ymd"))
				$partner = 'alfa';
		}
		else{
			if(strpos($thePixel,"adl") !== FALSE) { $partner = 'mobifreak'; }
			else if(strpos($thePixel,"bmconv") !== FALSE) { $partner = 'bmb'; }
			else if(strpos($thePixel,"test_offer") !== FALSE) { $partner = 'bmb'; }
		}

		return simplepay_invmodel_cmp::putPixel(array(
			 "pixel"		=> $thePixel
			,"operator"		=> $operator
			,"service"		=> $service
			,"partner"		=> $partner
			,"campurl"		=> $campurl
			,"msisdn"		=> $msisdn
		));
	}
	
	public static function buffer($p)
	{
		$log_profile = 'cmp_processor';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => 'Start : ' . json_encode($p)));
		
		$getPixel = simplepay_invmodel_cmp::getPixel(array(
			 "operator"		=> $p['operator']
			,"service"		=> $p['service']
			,"msisdn"		=> $p['msisdn']
		));
		
		if(count($getPixel) < 1)
		{
			$log->write(array('level' => 'debug', 'message' => "No pixel in pixel storage partner"));
		}
		else	
		{
			$log->write(array('level' => 'debug', 'message' => "Partner [".$storage[0]['partner']."], Msisdn [".$storage[0]['msisdn']."]"));

			$cmp_manager = new manager_cmp_processor();
			
			$cmp_manager->saveToBuffer(
				array(
					 'id'		=> $getPixel[0]['pixel']
					,'msisdn'	=> $p['msisdn']
					,'instid'	=> $p['msisdn']
					,'partner'	=> $getPixel[0]['partner']
					,'service'	=> $p['service']
					,'adn'		=> ''
					,'channel'	=> 'sms'
					,'operator'	=> $p['operator']
				)
			);
		}
	}
	
	public function process($arrdata,$hset_records)
	{
		$log = manager_logging::getInstance();
		
		$log->write(array('level' => 'debug', 'message' => "arrdata : " . print_r($arrdata, true) . ", hsetdata : " . print_r($hset_records, true)));
		
		$hmo = loader_model::getInstance()->load('hmo', 'cmp');
		$hset = loader_model::getInstance()->load('hset', 'cmp');
		$mdata['msgid'] = $arrdata['id'];
		$mdata['msisdn'] = $arrdata['instid'];

		$url = $hset_records['api_url'];
		$params = explode('|',$hset_records['params']);
		foreach($params as $idx => $row) {
				if(isset($mdata[$row])) {
						if($row=='msgData') {
								$url = str_replace('@'.$row.'@',urlencode($mdata[$row]),$url);
						} else {
								$url = str_replace('@'.$row.'@',$mdata[$row],$url);
						}
				} else {
						return true;
				}
		}

		$hmo_data = new model_data_hmo();
		$hset_data = new model_data_hset();
		$hmo_data->msisdn = $mdata['msisdn'];
		$hmo_data->date_send = date('Y-m-d');
		$hmo_data->time_send = date('H:i:s');
		$hmo_data->hash = $arrdata['id'];
		$hmo_data->hset_id = $hset_records['id'];
		$hmo_data->status = 0;
		
		if($hmo->isUnique($hmo_data)) 
		{	
			$hmo_data->status = 0;
			$hmo_data->closereason='NOK';
				
			$hset_data->inc = $hset_records['inc'];
			$hset_data->receive = $hset_records['receive']+1;
			
			if($hset_records['inc'] <= $hset_records['counter'] && $hset_records['inc'] <> 0)
			{
				$arrUrl = explode('?',$url);
				$url = $arrUrl[0];
				$prm = isset($arrUrl[1]) ? $arrUrl[1] : '';
				$hit = http_request::get($url, $prm, $hset_records['send_timeout']);
				$hit = trim(strtoupper($hit));
				//if($hit=='OK') {
						$hmo_data->status = 1;
				//}
				$hmo_data->closereason='OK';
				
				if($hset_records['inc'] == $hset_records['counter']){
					$hset_data->inc = $hset_records['ratio'];
					$hset_data->receive = 0;
				}
				else
					$hset_data->inc = $hset_records['inc']-1;
			} 
			else 
			{
				if($hset_data->receive >= $hset_records['counter']) 
				{
					$hset_data->receive = 0;
					$hset_data->inc = $hset_records['ratio'];
				}
			}
			
		} 
		else 
		{
			$hset_data->inc = $hset_records['inc'];
			$hmo_data->closereason = 'not unique';
		}
		
		$log->write(array('level' => 'debug', 'message' => "hmo data : " . print_r($hmo, true) . ", hset_data : " . print_r($hset_data, true)));
		
		if($hmo->save($hmo_data)) 
		{
			$hset_data->id = $hset_records['id'];
			$hset->update($hset_data);
		}

		return true;
	}
}
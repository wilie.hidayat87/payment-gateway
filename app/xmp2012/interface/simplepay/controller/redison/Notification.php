<?php
class simplepay_controller_redison_Notification
{
	public static function dr()
	{
		$bodyRequest = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		
		$log_profile = 'redison_order_completed_receiver';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => 'NOTIF REDISON : ' . $bodyRequest));
		
		/*
		stdClass Object
		(
			[app_id] => 5ee093e0764f1bfb0f8b45ab
			[client_appkey] => 8VFKEjWOjcXk6RTM7L2cAQdmutFlR0CZ
			[user_id] => 628121333064
			[user_ip] => 114.124.169.162
			[user_mdn] => 628121333064
			[merchant_transaction_id] => 2020061118163115918741915056
			[transaction_description] => 
			[payment_method] => telkomsel_airtime
			[currency] => IDR
			[amount] => 5000
			[status_code] => 1000
			[status] => payment_completed
			[item_id] => 1
			[item_name] => ORDER ITEM 1
			[updated_at] => 1591874252
			[reference_id] => 5ee2128f109f52a0b48b45fd
			[testing] => 1
			[custom] => 
		)
		*/
		
		$configRedison = loader_config::getInstance()->getConfig('redison');
		
		$parameters = json_decode($bodyRequest);
		
		// Get Transaction by Merchant ID
		
		$rows = simplepay_invmodel_get::getTransaction(array(
			 'userid' 				=> $parameters->user_id
			,'payment_gateway' 		=> $configRedison->payment_gateway
			,'payment_channel' 		=> $parameters->payment_method
			,'currency' 			=> $parameters->currency
			,'amount' 				=> $parameters->amount
			,'item_id' 				=> $parameters->item_id
			,'item_name' 			=> $parameters->item_name
			,'merchant_transid' 	=> $parameters->merchant_transaction_id
		));
		
		if(count($rows) > 0)
		{
			// Save transaction Order Payment Confirmation in the database
				
			simplepay_invmodel_set::updateTransaction(array(
				 'payment_gateway' 		=> $configRedison->payment_gateway
				,'app_name' 			=> $rows[0]['app_name']
				,'userid' 				=> $parameters->user_id
				,'payment_channel' 		=> $parameters->payment_method
				,'currency' 			=> $parameters->currency
				,'amount' 				=> $parameters->amount
				,'item_id' 				=> $parameters->item_id
				,'item_name' 			=> $parameters->item_name
				,'merchant_transid' 	=> $parameters->merchant_transaction_id
				,'time_confirmed_order' => date("Y-m-d H:i:s", $parameters->updated_at)
				,'status_code' 			=> $parameters->status_code
				,'reason_code' 			=> $parameters->status
				,'transact_desc' 		=> "Refid:" . $parameters->reference_id
				,'reserve1' 			=> $rows[0]['reserve1']
				,'reserve2' 			=> $rows[0]['reserve2']
				,'reserve3' 			=> $rows[0]['reserve3']
			));
			
			// Get Subscription data by user id & app name
			// - Adding subscription method when receive notification payment
			
			$active = 0;
			if($parameters->status == '1000' && strtoupper($parameters->status_code) == "PAYMENT_COMPLETED")
				$active = 1;
			
			$subs = simplepay_invmodel_get::getSubs(array(
				 'app_name' => $rows[0]['app_name']
				,'userid' 	=> $parameters->user_id
			));
			
			//print_r($subs);die;
			if(count($subs) > 0){
				
				// Update Subscription data when subs is not empty
					
				simplepay_invmodel_set::updateSubs(array(
					 'payment_gateway' 			=> $configRedison->payment_gateway
					,'app_name' 				=> $rows[0]['app_name']
					,'userid' 					=> $parameters->user_id
					,'payment_channel' 			=> $parameters->payment_method
					,'currency' 				=> $parameters->currency
					,'amount' 					=> $parameters->amount
					,'active' 					=> $active
				));
				
			}else{
				
				// Otherwise insert into subscription table when is empty
				
				simplepay_invmodel_set::insertSubs(array(
					 'payment_gateway' 			=> $configRedison->payment_gateway
					,'app_name' 				=> $rows[0]['app_name']
					,'userid' 					=> $parameters->user_id
					,'payment_channel' 			=> $parameters->payment_method
					,'currency' 				=> $parameters->currency
					,'amount' 					=> $parameters->amount
					,'active' 					=> $active
				));
			}
			
			// Filter by app name for requirement project
			
			switch($rows[0]['app_name'])
			{
				case 'SURATSAKIT' :
					
					$req = array();
					
					$req['body'] = json_encode(array(
						 'trx_id' 	=> $parameters->merchant_transaction_id
						,'user_id'	=> $parameters->user_id
						,'status'	=> $parameters->status
						,'reason' 	=> $parameters->status_code
						,'datetime' => date("YmdHis", $parameters->updated_at)
					));

					$req['url'] = "https://suratsakit.com/action/dr-payment-api.php";
					$req['port'] = 0;
					$req['login'] = "";
					$req['ssl'] = false;
					$req['timeout'] = 5;
					$req['headers'] = array(
						'Content-Type: application/json',
						/* 'Accept-Encoding: gzip, deflate', */
						'Cache-Control: max-age=0',
						'Connection: keep-alive',
						'Accept-Language: en-US,en;q=0.8,id;q=0.6',
						'APPNAME: ' . $rows[0]['app_name'],
						'AUTH_USER: ' . $rows[0]['app_name'],
						'AUTH_PWD: '.md5('suratsakitPass2020'),
					);
					
					http_request::requestPost($req, "NOTIF REDISON/SURATSAKIT");
				
				break;
				
				case 'SPARSA' :
					
					$req = array();
					
					$req['body'] = json_encode(array(
						 'trx_id' 	=> $parameters->merchant_transaction_id
						,'user_id'	=> $parameters->user_id
						,'status'	=> $parameters->status
						,'reason' 	=> $parameters->status_code
						,'datetime' => date("YmdHis", $parameters->updated_at)
					));

					$req['url'] = "http://sparsa.id/mobile/action/dr-payment-api.php";
					$req['port'] = 0;
					$req['login'] = "";
					$req['ssl'] = false;
					$req['timeout'] = 5;
					$req['headers'] = array(
						'Content-Type: application/json',
						/* 'Accept-Encoding: gzip, deflate', */
						'Cache-Control: max-age=0',
						'Connection: keep-alive',
						'Accept-Language: en-US,en;q=0.8,id;q=0.6',
						'APPNAME: ' . $rows[0]['app_name'],
						'AUTH_USER: ' . $rows[0]['app_name'],
						'AUTH_PWD: '.md5('SparsaPass2020'),
					);
					
					http_request::requestPost($req, "NOTIF REDISON/SPARSA");
				
				break;
				
				default : 
					
				break;
			}
		}
		
		return $result;
	}
}
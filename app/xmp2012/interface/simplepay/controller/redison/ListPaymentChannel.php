<?php
class simplepay_controller_redison_ListPaymentChannel
{
	public static function get()
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => 'Start'));
		
		$configMain = loader_config::getInstance()->getConfig('main');
		
		// Get cache name file
						
		$file_cache = $configMain->cachePath . "/api_list_payment_channel.json";
		
		// Get cache file

		$currData = file_process::get($file_cache);

		// Check whether there is content in a file
		
		if(empty($currData))
		{	
			// Get payment list channel in the database
			
			$rows = simplepay_invmodel_get::getListPaymentChannel();
			
			// Set data payment channel list in the file
			// Set expire time in this file
			
			file_process::save($file_cache, json_encode(
				array
				(
					 "exp_date" => strtotime($configMain->cacheExp)
					,"data" 	=> serialize($rows)
				)
			));
			
			// Set Acknowledge to client
			
			$result = json_encode($rows);
		}
		else
		{
			// Parse data into decoder json type from file content
			
			$currData = json_decode($currData);
			
			if(is_object($currData))
			{
				// Time expire file cache system checking base on setting
				
				if(time() >= (int)$currData->exp_date)
				{
					// Get payment list channel in the database
					
					$rows = simplepay_invmodel_get::getListPaymentChannel();
					
					// Set data payment channel list in the file
					// Set expire time in this file
			
					file_process::save($file_cache, json_encode(
						array
						(
							 "exp_date" => strtotime($configMain->cacheExp)
							,"data" 	=> serialize($rows)
						)
					));
					
					// Set Acknowledge to client
					
					$result = json_encode($rows);
				}
				else
				{
					// Set Acknowledge to client
					
					$result = json_encode(unserialize($currData->data));
				}
			}
		}
		
		return $result;
	}
}
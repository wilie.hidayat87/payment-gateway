<?php
class simplepay_controller_redison_Payment
{
	public static function order()
	{
		$bodyRequest = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		
		$log_profile = 'redison_order_processor';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => 'Start : ' . $bodyRequest));
						
		$checkJsonParams = http_validate::jsonValidate($bodyRequest);
		
		$log->write(array('level' => 'info', 'message' => 'Request Checker : ' . print_r($checkJsonParams,1)));
		
		if($checkJsonParams['error'] === FALSE)
		{					
			$params = $checkJsonParams['result'];
			
			$msisdn = (int)trim($params->msisdn);
			$payment_channel = trim($params->payment_channel);
			$currency = trim($params->currency);
			$amount = (int)trim($params->amount);
			$item_id = (int)trim($params->item_id);
			$item_name = trim($params->item_name);
			
			if(empty($msisdn))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : msisdn not found!'));
			}
			else if(empty($payment_channel))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : payment_channel not found!'));
			}
			else if(empty($currency))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : currency not found!'));
			}
			else if(empty($amount))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : amount not found!'));
			}
			else if(empty($item_id))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : item_id not found!'));
			}
			else if(empty($item_name))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : item_name not found!'));
			}
			else
			{
				$requestHit = simplepay_module_redison::orderPayment(array(
					 "app_name"			=> trim($_SERVER['HTTP_APPNAME'])
					,"msisdn"			=> $msisdn
					,"payment_channel"	=> $payment_channel
					,"currency"			=> $currency
					,"amount"			=> $amount
					,"item_id"			=> $item_id
					,"item_name"		=> $item_name
				));
				
				http_response_code((int)$requestHit['header_code']);
				
				// CPA CAMPAIGN CHECKER TO PIXEL STORAGE
				
				if((int)$requestHit['header_code'] == 200 && $params->custom == "campaign")
				{
					simplepay_controller_managePixel::buffer(array(
						"operator" 	=> $payment_channel."_".$currency."_".$amount, 
						"msisdn" 	=> $msisdn, 
						"service" 	=> trim($_SERVER['HTTP_APPNAME'])
					));
				}
				
				return $requestHit['output'];
			}
		}
		else
		{
			$log->write(array('level' => 'info', 'message' => 'Start'));
		
			http_response_code(400);
			
			$result = json_encode(array('err' => 'Bad Request, check your parameters'));
		}
						
		return $result;
	}
}
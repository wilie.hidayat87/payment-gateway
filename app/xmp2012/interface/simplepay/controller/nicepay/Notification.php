<?php
class simplepay_controller_nicepay_Notification
{
	public static function fetch()
	{	
		$log_profile = 'nicepay_order_completed_receiver';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
		
		$conf = loader_config::getInstance()->getConfig('nicepay');

		$bodyRequest = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		
        try 
		{		  
			simplepay_module_nicepay::notification($bodyRequest);
				
			http_response_code(200);
			
		} catch (Exception $e) {
		  
		  $log->write(array('level' => 'info', 'message' => "Exception: ".$e->getMessage().", Notification received: ".$bodyRequest));
		} 		
		
		return true;
	}
}
<?php
class simplepay_controller_nicepay_Payment
{
	public static function order()
	{
		$bodyRequest = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
		
		$log_profile = 'nicepay_order_processor';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => 'Start : ' . $bodyRequest));
						
		$checkJsonParams = http_validate::jsonValidate($bodyRequest);
		
		$log->write(array('level' => 'info', 'message' => 'Request Checker : ' . print_r($checkJsonParams,1)));
		
		if($checkJsonParams['error'] === FALSE)
		{					
			$params = $checkJsonParams['result'];
			
			$gross_amount = trim($params->gross_amount);
			$first_name = trim($params->first_name);
			$last_name = trim($params->last_name);
			$email = trim($params->email);
			$phone = trim($params->phone);
			$via = trim($params->via);
			$userip = trim($params->userip);
			
			if(empty($gross_amount))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : gross_amount not found!'));
			}
			else if(empty($first_name))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : first_name not found!'));
			}
			else if(empty($last_name))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : last_name not found!'));
			}
			else if(empty($email))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : email not found!'));
			}
			else if(empty($phone))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : phone not found!'));
			}
			else if(empty($via))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : via not found!'));
			}
			else if(empty($userip))
			{
				http_response_code(400);
				return json_encode(array('err' => 'Bad Request : userip not found!'));
			}
			else
			{
				$requestHit = simplepay_module_nicepay::orderPayment(array(
					 "app_name"		=> trim($_SERVER['HTTP_APPNAME'])
					,"gross_amount"	=> $gross_amount
					,"first_name"	=> $first_name
					,"last_name"	=> $last_name
					,"email"		=> $email
					,"phone"		=> $phone
					,"via"			=> $via
					,"userip"		=> $userip
				));
				
				http_response_code((int)$requestHit['header_code']);
				
				// CPA CAMPAIGN CHECKER TO PIXEL STORAGE
				
				if((int)$requestHit['header_code'] == 200 && $params->custom == "campaign")
				{
					simplepay_controller_managePixel::buffer(array(
						"operator" 	=> "nicepay_IDR_".$gross_amount, 
						"msisdn" 	=> $phone, 
						"service" 	=> trim($_SERVER['HTTP_APPNAME'])
					));
				}
				
				return $requestHit['output'];
			}
		}
		else
		{
			$log->write(array('level' => 'info', 'message' => 'Start'));
		
			http_response_code(400);
			
			$result = json_encode(array('err' => 'Bad Request, check your parameters'));
		}
						
		return $result;
	}
}
#!/usr/bin/php
<?php

$params = getopt('t:n:');

if(!isset($params['t']) && !isset($params['n'])) {
	echo 'Incomplete parameter. Usage' . "\n";
	echo 'payment.php -t 0 -n tpm' . "\n";
	exit(0);
}

require_once '/app/xmp2012/interface/simplepay/xmp.php';

$result = simplepay_controller_bniva_Payment::processPayment();

simplepay_invmodel_set::closeConn();

if ($result) {
	echo "OK \n";
} else {
	echo "NOK \n";
}

exit(0);
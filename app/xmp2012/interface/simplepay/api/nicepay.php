<?php
class simplepay_api_nicepay
{
    public static function get($api_name)
	{
		// Let system refreshed at certain micro seconds by api requested
		
		usleep(100);
		
		$log_profile = 'nicepay_api';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => 'Start'));
		
		switch($api_name)
		{
			/*  ===
				API NAME 		: orderPayment
				Method			: POST
				Host			: http://103.77.79.13:8575
				Endpoint		: /api/nicepay/orderPayment/
				Response Type 	: JSON
				Description 	:
				
					- Request order payment via channel and amount of price
				
				Request Params ( in json )	:
				
				+----------------------------+---------------------+---------------------+----------------+
				| Field                      | Type (length)       | Requirement         | Extra          |
				+----------------------------+---------------------+---------------------+----------------+
				| app_name	                 | string(50)          | Mandatory           |                |
				| gross_amount               | int(20)             | Mandatory           |                |
				| first_name     	         | string(100)         | Mandatory           |                |
				| last_name                  | string(100)         | Mandatory           |                |
				| email                      | string(50)          | Mandatory           |                |
				| phone		             	 | string(25)          | Mandatory           |                |
				| via		            	 | string(25)          | Mandatory           | eg: NICEPAY    |
				| userip		             | string(25)          | Mandatory           |                |
				| custom		             | string(25)          | Mandatory           |                |
				+----------------------------+---------------------+---------------------+----------------+

				===
			*/
			
			// API Name 
			
			case 'orderPayment' :
			
				if (isset($_REQUEST)) 
				{
					$requets_method = $_SERVER['REQUEST_METHOD'];
					
					if($requets_method == "POST")
					{	
						// Request list in controller
						
						$result = simplepay_controller_nicepay_Payment::order();
						
						header("Content-Type: application/json");
						
						echo json_encode($result);
					}
					else 
					{
						$log->write(array('level' => 'info', 'message' => 'Start'));
						
						http_response_code(405);
						header("Content-Type: application/json");
						
						echo json_encode(array('err' => 'Request method not allowed'));
					}
				}
				else
				{
					$log->write(array('level' => 'info', 'message' => 'Start'));
						
					http_response_code(400);
					header("Content-Type: application/json");
					
					echo json_encode(array('err' => 'Bad Request'));
				}
				
			break;
			
			/*  ===
				API NAME 		: notification
				Method			: POST
				Host			: http://103.77.79.13:8575
				Endpoint		: /nicepay/notif/
				Response Type 	: plain/text
				Description 	:
				===
			*/
			
			case 'notification' :
			
				if (isset($_REQUEST)) 
				{
					/* $requets_method = $_SERVER['REQUEST_METHOD'];
					
					if($requets_method == "POST")
					{ */	
						// Request list in controller

						simplepay_controller_nicepay_Notification::fetch();
				
						header("Content-Type: application/x-www-form-urlencoded");
						echo "OK";
					/* }
					else  
					{
						$log->write(array('level' => 'info', 'message' => 'Start'));
						
						http_response_code(405);
						header("Content-Type: application/x-www-form-urlencoded");
						
						echo 'Request method not allowed';
					} */
				}
				else
				{
					$log->write(array('level' => 'info', 'message' => 'Start'));
						
					http_response_code(400);
					header("Content-Type: application/x-www-form-urlencoded");
					
					echo 'Bad Request';
				}
				
			break;
		
			default : 
			
				http_response_code(503);
				$log->write(array('level' => 'debug', 'message' => 'Service Not Available'));
				return true; 
				
			break;
		}
		
		exit();
	}
}
<?php
class simplepay_api_bniva
{
    public static function get($api_name)
	{
		// Let system refreshed at certain micro seconds by api requested
		
		usleep(100);
		
		$log_profile = 'bniva_api';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => 'Start Params : ' . print_r($_REQUEST, 1) . ', input body : ' . file_get_contents('php://input')));
		
		switch($api_name)
		{			
			// **** REQUEST PAYMENT VIA VIRTUAL ACCOUNT BNI ***** //
			
			/*  ===
				API NAME 		: requestVA
				Method			: POST
				Host			: http://103.77.79.13:8575
				Endpoint		: /api/cbB/requestVA/
				Headers 		: Content-type: application/json, APPNAME: {STRING}, ex: TESTING/SPARSA/JOJOKU
				Response Type 	: JSON
				Description 	:
				
					- Request order payment via channel and amount of price
				
				Request Params ( in json )	:
				
				+----------------------------+---------------------+---------------------+----------------+
				| Field                      | Type (length)       | Requirement         | Extra          |
				+----------------------------+---------------------+---------------------+----------------+
				| custname                   | string(50)          | Mandatory           |                |
				| msisdn                     | int(20)             | Mandatory           |                |
				| email                      | string(30)          | Mandatory           |                |
				| currency                   | string(3)           | Mandatory           |                |
				| amount                     | int(10)             | Mandatory           |                |
				| custom                     | string(30)          | Optional            |                |
				+----------------------------+---------------------+---------------------+----------------+

				===
			*/
			
			case 'requestbniva' :
			
				if (isset($_REQUEST)) 
				{
					$requets_method = $_SERVER['REQUEST_METHOD'];
					
					if($requets_method == "POST")
					{	
						// Controller
						$result = simplepay_controller_bniva_Payment::order();
						
						header("Content-Type: application/json");
						
						echo $result;
					}
					else 
					{
						$log->write(array('level' => 'info', 'message' => 'Start'));
						
						http_response_code(405);
						header("Content-Type: application/json");
						
						echo json_encode(array('err' => 'Request method not allowed'));
					}
				}
				else
				{
					$log->write(array('level' => 'info', 'message' => 'Start'));
						
					http_response_code(400);
					header("Content-Type: application/json");
					
					echo json_encode(array('err' => 'Bad Request'));
				}
				
			break;
			
			/*  ===
				API NAME 		: notification
				Method			: POST
				Host			: http://103.77.79.13:8575
				Endpoint		: /api/cbB/notification
				Response Type 	: JSON
				ACK Requirement : OK
				Description 	:
				
					- Delivery Report for payment notification
				
				Receive Params ( in json )	:
				
					
				
					- Response should look like this : 
					
					{"status":"000"}
				===
			*/
			
			case 'notification' :
			
				//$xml = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
				
				// Request list in controller

				$result = simplepay_controller_bniva_Notification::dr();
				
				header("Content-Type: application/json");
						
				echo json_encode($result); 
				
			break;
			
			// **** DEFAULT STRUCT API STATE ***** //
			
			/* case 'APINAME' :
			
				if (isset($_REQUEST)) 
				{
					$requets_method = $_SERVER['REQUEST_METHOD'];
					
					if($requets_method == "POST")
					if($requets_method == "GET")
					if($requets_method == "PUT")
					if($requets_method == "DELETE")
					{	
				
					}
					else 
					{
						$log->write(array('level' => 'info', 'message' => 'Start'));
						
						http_response_code(405);
						header("Content-Type: application/json");
						
						echo json_encode(array('err' => 'Request method not allowed'));
					}
				}
				else
				{
					$log->write(array('level' => 'info', 'message' => 'Start'));
						
					http_response_code(400);
					header("Content-Type: application/json");
					
					echo json_encode(array('err' => 'Bad Request'));
				}
				
			break; */
			
			default : 
			
				http_response_code(503);
				$log->write(array('level' => 'debug', 'message' => 'Service Not Available'));
				return true; 
				
			break;
		}
		
		exit();
	}
}
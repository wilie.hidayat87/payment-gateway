<?php
class simplepay_api_achiko
{
    public static function get($api_name)
	{
		// Let system refreshed at certain micro seconds by api requested
		
		usleep(100);
		
		$log_profile = 'achiko_api';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => 'Start Params : ' . print_r($_REQUEST, 1) . ', input body : ' . file_get_contents('php://input')));
		
		switch($api_name)
		{

			/*  ===
				API NAME 		: getListPaymentChannel
				Method			: GET
				Host			: http://103.77.79.13:8575
				Endpoint		: /api/achiko/getListPaymentChannel/
				Response Type 	: JSON
				Description 	:
				
					- Get all payment channel list
					- Implement cache to prevent hard load
				
				===
			*/
			
			// API Name 
			
			case 'getListPaymentChannel' :
			
				if (isset($_REQUEST)) 
				{
					// Get all request method in SERVER GLOBAL PHP
					
					$requets_method = $_SERVER['REQUEST_METHOD'];
					
					if($requets_method == "GET")
					{	
						// Request list in controller
						
						$result = simplepay_controller_achiko_ListPaymentChannel::get();
						
						http_response_code(200);
						header("Content-Type: application/json");
						
						echo $result;
					}
					else 
					{
						$log->write(array('level' => 'info', 'message' => 'Start'));
						
						http_response_code(405);
						header("Content-Type: application/json");
						
						echo json_encode(array('err' => 'Request method not allowed'));
					}
				}
				else
				{
					$log->write(array('level' => 'info', 'message' => 'Start'));
						
					http_response_code(400);
					header("Content-Type: application/json");
					
					echo json_encode(array('err' => 'Bad Request'));
				}
				
			break;
			
			/*  ===
				API NAME 		: orderPayment
				Method			: POST
				Host			: http://103.77.79.13:8575
				Endpoint		: /api/achiko/orderPayment/
				Response Type 	: JSON
				Description 	:
				
					- Request order payment via channel and amount of price
				
				Request Params ( in json )	:
				
				+----------------------------+---------------------+---------------------+----------------+
				| Field                      | Type (length)       | Requirement         | Extra          |
				+----------------------------+---------------------+---------------------+----------------+
				| msisdn                     | int(20)             | Mandatory           |                |
				| payment_channel            | string(30)          | Mandatory           |                |
				| currency                   | string(3)           | Mandatory           |                |
				| amount                     | int(10)             | Mandatory           |                |
				| item_id		             | int(10)             | Mandatory           |                |
				| item_name			         | string(30)          | Mandatory           |                |
				| custom            		 | string(100)         | Optional            |                |
				+----------------------------+---------------------+---------------------+----------------+

				===
			*/
			
			// API Name 
			
			case 'orderPayment' :
			
				if (isset($_REQUEST)) 
				{
					$requets_method = $_SERVER['REQUEST_METHOD'];
					
					if($requets_method == "POST")
					{	
						// Request list in controller
						
						$result = simplepay_controller_achiko_Payment::order();
						
						header("Content-Type: application/json");
						
						echo $result;
					}
					else 
					{
						$log->write(array('level' => 'info', 'message' => 'Start'));
						
						http_response_code(405);
						header("Content-Type: application/json");
						
						echo json_encode(array('err' => 'Request method not allowed'));
					}
				}
				else
				{
					$log->write(array('level' => 'info', 'message' => 'Start'));
						
					http_response_code(400);
					header("Content-Type: application/json");
					
					echo json_encode(array('err' => 'Bad Request'));
				}
				
			break;
			
			/*  ===
				API NAME 		: receivePaymentConfirmation
				Method			: POST
				Host			: http://103.77.79.13:8575
				Endpoint		: /notif/sandbox/ (development) or /notif/production/ (production)
				Response Type 	: JSON
				ACK Requirement : OK
				Description 	:
				
					- Delivery Report for payment notification
				
				Receive Params ( in json )	:
				
				+-----------------------------------------+----------------+
				| Field Object               			  | Type           |
				+-----------------------------------------+----------------+
				| data->id                   			  | int            |
				| data->timestamp            			  | unix_timestamp |
				| data->details->app_id            		  | int		       |
				| data->details->user_id            	  | int		       |
				| data->details->merchant_transaction_id  | int		       |
				| data->details->transaction_description  | string         |
				| data->details->payment_channel  		  | string         |
				| data->details->channel_name  			  | string         |
				| data->details->currency            	  | string		   |
				| data->details->amount            		  | string		   |
				| data->details->status_code              | string		   |
				| data->details->status           		  | string		   |
				| data->details->item_id           		  | string		   |
				| data->details->item_name           	  | string		   |
				| data->details->testing           		  | string		   |
				| data->details->custom           		  | string		   |
				+-----------------------------------------+----------------+

				===
			*/
			
			// API Name
			
			case 'receivePaymentConfirmation' :
			
				if (isset($_REQUEST)) 
				{
					$requets_method = $_SERVER['REQUEST_METHOD'];
					
					if($requets_method == "POST")
					{	
						$result = simplepay_controller_achiko_ReceivePaymentConfirmation::dr();
						
						http_response_code(200);
						header("Content-Type: text/plain");
						
						echo "OK";
					}
					else 
					{
						$log->write(array('level' => 'info', 'message' => 'Start'));
						
						http_response_code(405);
						header("Content-Type: application/json");
						
						echo json_encode(array('err' => 'Request method not allowed'));
					}
				}
				else
				{
					$log->write(array('level' => 'info', 'message' => 'Start'));
						
					http_response_code(400);
					header("Content-Type: application/json");
					
					echo json_encode(array('err' => 'Bad Request'));
				}
				
			break; 
			
			/*  ===
				API NAME 		: orderChecking
				Method			: POST
				Host			: http://103.77.79.13:8575
				Endpoint		: /api/achiko/orderChecking/
				Response Type 	: JSON
				ACK Requirement : OK
				Description 	:
				
					- Delivery Report for payment notification
				
				Receive Params ( in json )	:
				
				+-----------------------------------------+----------------+
				| Field Object               			  | Type           |
				+-----------------------------------------+----------------+
				| id                   			  		  | int            |
				| timestamp            			  		  | unix_timestamp |
				+-----------------------------------------+----------------+

				===
			*/
			
			// API Name
			
			case 'orderChecking' :
			
				if (isset($_REQUEST)) 
				{
					$requets_method = $_SERVER['REQUEST_METHOD'];
					
					if($requets_method == "POST")
					{	
						$result = simplepay_controller_achiko_OrderChecking::check();
						
						header("Content-Type: application/json");
						
						echo $result;
					}
					else 
					{
						$log->write(array('level' => 'info', 'message' => 'Start'));
						
						http_response_code(405);
						header("Content-Type: application/json");
						
						echo json_encode(array('err' => 'Request method not allowed'));
					}
				}
				else
				{
					$log->write(array('level' => 'info', 'message' => 'Start'));
						
					http_response_code(400);
					header("Content-Type: application/json");
					
					echo json_encode(array('err' => 'Bad Request'));
				}
				
			break;
			
			// **** DEFAULT STRUCT API STATE ***** //
			
			/* case 'APINAME' :
			
				if (isset($_REQUEST)) 
				{
					$requets_method = $_SERVER['REQUEST_METHOD'];
					
					if($requets_method == "POST")
					if($requets_method == "GET")
					if($requets_method == "PUT")
					if($requets_method == "DELETE")
					{	
				
					}
					else 
					{
						$log->write(array('level' => 'info', 'message' => 'Start'));
						
						http_response_code(405);
						header("Content-Type: application/json");
						
						echo json_encode(array('err' => 'Request method not allowed'));
					}
				}
				else
				{
					$log->write(array('level' => 'info', 'message' => 'Start'));
						
					http_response_code(400);
					header("Content-Type: application/json");
					
					echo json_encode(array('err' => 'Bad Request'));
				}
				
			break; */
			
			default : 
			
				http_response_code(503);
				$log->write(array('level' => 'debug', 'message' => 'Service Not Available'));
				return true; 
				
			break;
		}
		
		exit();
	}
}
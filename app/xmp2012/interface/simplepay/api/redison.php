<?php
class simplepay_api_redison
{
    public static function get($api_name)
	{
		// Let system refreshed at certain micro seconds by api requested
		
		usleep(100);
		
		$log_profile = 'redison_api';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => 'Start'));
		
		switch($api_name)
		{
			/*  ===
				API NAME 		: getListPaymentChannel
				Method			: GET
				Host			: http://103.77.79.13:8575
				Endpoint		: /api/cbRedi/getListPaymentChannel/
				Response Type 	: JSON
				Description 	:
				
					- Get all payment channel list
					- Implement cache to prevent hard load
				
				===
			*/
			
			// API Name 
			
			case 'getListPaymentChannel' :
			
				if (isset($_REQUEST)) 
				{
					// Get all request method in SERVER GLOBAL PHP
					
					$requets_method = $_SERVER['REQUEST_METHOD'];
					
					if($requets_method == "GET")
					{	
						// Request list in controller
						
						$result = simplepay_controller_redison_ListPaymentChannel::get();
						
						http_response_code(200);
						header("Content-Type: application/json");
						
						echo $result;
					}
					else 
					{
						$log->write(array('level' => 'info', 'message' => 'Start'));
						
						http_response_code(405);
						header("Content-Type: application/json");
						
						echo json_encode(array('err' => 'Request method not allowed'));
					}
				}
				else
				{
					$log->write(array('level' => 'info', 'message' => 'Start'));
						
					http_response_code(400);
					header("Content-Type: application/json");
					
					echo json_encode(array('err' => 'Bad Request'));
				}
				
			break;
			
			/*  ===
				API NAME 		: orderPayment
				Method			: POST
				Host			: http://103.77.79.13:8575
				Endpoint		: /api/cbRedi/orderPayment/
				Headers 		: Content-type: application/json, APPNAME: {STRING}, ex: TESTING/SPARSA/JOJOKU
				Response Type 	: JSON
				Description 	:
				
					- Request order payment via channel and amount of price
				
				Request Params ( in json )	:
				
				+----------------------------+---------------------+---------------------+----------------+
				| Field                      | Type (length)       | Requirement         | Extra          |
				+----------------------------+---------------------+---------------------+----------------+
				| msisdn                     | int(20)             | Mandatory           |                |
				| payment_channel            | string(30)          | Mandatory           |                |
				| currency                   | string(3)           | Mandatory           |                |
				| amount                     | int(10)             | Mandatory           |                |
				| item_id		             | int(10)             | Mandatory           |                |
				| item_name			         | string(30)          | Mandatory           |                |
				| custom            		 | string(100)         | Optional            |                |
				+----------------------------+---------------------+---------------------+----------------+

				===
			*/
			
			// API Name 
			
			case 'orderPayment' :
			
				if (isset($_REQUEST)) 
				{
					$requets_method = $_SERVER['REQUEST_METHOD'];
					
					if($requets_method == "POST")
					{	
						// Request list in controller
						
						$result = simplepay_controller_redison_Payment::order();
						
						header("Content-Type: application/json");
						
						echo json_encode($result);
					}
					else 
					{
						$log->write(array('level' => 'info', 'message' => 'Start'));
						
						http_response_code(405);
						header("Content-Type: application/json");
						
						echo json_encode(array('err' => 'Request method not allowed'));
					}
				}
				else
				{
					$log->write(array('level' => 'info', 'message' => 'Start'));
						
					http_response_code(400);
					header("Content-Type: application/json");
					
					echo json_encode(array('err' => 'Bad Request'));
				}
				
			break;
			
			/*  ===
				API NAME 		: notification
				Method			: POST
				Host			: http://103.77.79.13:8575
				Endpoint		: /api/cbRedi/notification
				Response Type 	: JSON
				ACK Requirement : OK
				Description 	:
				
					- Delivery Report for payment notification
				
				Receive Params ( in json )	:
				{"app_id":"5ee093e0764f1bfb0f8b45ab","client_appkey":"8VFKEjWOjcXk6RTM7L2cAQdmutFlR0CZ","user_id":628121333064,"user_ip":"114.124.169.162","user_mdn":"628121333064","merchant_transaction_id":"2020061118163115918741915056","transaction_description":"","payment_method":"telkomsel_airtime","currency":"IDR","amount":5000,"status_code":"1000","status":"payment_completed","item_id":1,"item_name":"ORDER ITEM 1","updated_at":"1591874252","reference_id":"5ee2128f109f52a0b48b45fd","testing":"1","custom":""}
				
				===
			*/
			
			case 'notification' :
			
				$xml = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');
				
				// Request list in controller

				simplepay_controller_redison_Notification::dr();
				
				return true; 
				
			break;
			
			// **** DEFAULT STRUCT API STATE ***** //
			
			/* case 'APINAME' :
			
				if (isset($_REQUEST)) 
				{
					$requets_method = $_SERVER['REQUEST_METHOD'];
					
					if($requets_method == "POST")
					if($requets_method == "GET")
					if($requets_method == "PUT")
					if($requets_method == "DELETE")
					{	
				
					}
					else 
					{
						$log->write(array('level' => 'info', 'message' => 'Start'));
						
						http_response_code(405);
						header("Content-Type: application/json");
						
						echo json_encode(array('err' => 'Request method not allowed'));
					}
				}
				else
				{
					$log->write(array('level' => 'info', 'message' => 'Start'));
						
					http_response_code(400);
					header("Content-Type: application/json");
					
					echo json_encode(array('err' => 'Bad Request'));
				}
				
			break; */
		
			default : 
			
				http_response_code(503);
				$log->write(array('level' => 'debug', 'message' => 'Service Not Available'));
				return true; 
				
			break;
		}
		
		exit();
	}
}
<?php
class simplepay_module_midtrans
{
    public static function orderPayment($params = array())
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . print_r($params, true)));
			
		$midtransConf = loader_config::getInstance()->getConfig('midtrans');
		
		require_once $midtransConf->_3rdpartyPath . '/Midtrans.php';

		//Set Your server key
		\Midtrans\Config::$serverKey = $midtransConf->serverKey;

		// Uncomment for production environment
		if($midtransConf->env == "development")
			\Midtrans\Config::$isProduction = false;
		else
			\Midtrans\Config::$isProduction = true;

		// Enable sanitization
		\Midtrans\Config::$isSanitized = true;

		// Enable 3D-Secure
		\Midtrans\Config::$is3ds = true;
		
		$datetime = date ( "Y-m-d H:i:s" );
		$timestamp = time();
		$transid = str_replace(" ", "", str_replace(":", "", str_replace("-", "", $datetime))) . str_replace ( '.', '', microtime ( true ) );
		
		// Required
		$transaction_details = array(
			'order_id' => $transid,
			'gross_amount' => (isset($params['gross_amount']) ? $params['gross_amount'] : 10000), // no decimal allowed for creditcard
		);
		
		// Optional
		$customer_details = array(
			'first_name'    => (isset($params['first_name']) ? $params['first_name'] : 'default'),
			'last_name'     => (isset($params['last_name']) ? $params['last_name'] : 'default'),
			'email'         => (isset($params['email']) ? $params['email'] : ''),
			'phone'         => (isset($params['phone']) ? (substr($params['phone'],0,1) == '0' ? "62" . substr($params['phone'],1,strlen($params['phone'])) : $params['phone']) : ''),
		);

		// Fill transaction details
		$transaction = array(
			'transaction_details' 	=> $transaction_details,
			'customer_details' 		=> $customer_details,
		);

		$snapToken = \Midtrans\Snap::getSnapToken($transaction);
		
		// Save transaction Order Payment in the database
		
		$resp = array();
		
		if(count(explode("-",$snapToken)) == 5){
			$resp['header_code'] = 200;
			$resp['output'] = 
				array(
					"orderid" => $transaction_details['order_id'],
					"snapJSURL" => $midtransConf->snapJSURL, 
					"paymentPage" => $midtransConf->paymentPage . '/' . $snapToken, 
					"clientKey" => $midtransConf->clientKey, 
					"token" => $snapToken, 
					"desc" => "Success obtain Snap token."
				);
		}
		if(strpos($snapToken,"transaction_details") !== FALSE){
			$resp['header_code'] = 400;
			$resp['output'] = 
				array(
					"orderid" => $transaction_details['order_id'],
					"snapJSURL" => $midtransConf->snapJSURL, 
					"paymentPage" => $midtransConf->paymentPage . '/' . $snapToken, 
					"clientKey" => $midtransConf->clientKey, 
					"token" => $snapToken, 
					"desc" => "Failed. Wrong parameter."
				);
		}
		if(strpos($snapToken,"Access denied") !== FALSE){
			$resp['header_code'] = 401;
			$resp['output'] = 
				array(
					"orderid" => $transaction_details['order_id'],
					"snapJSURL" => $midtransConf->snapJSURL, 
					"paymentPage" => $midtransConf->paymentPage . '/' . $snapToken, 
					"clientKey" => $midtransConf->clientKey, 
					"token" => $snapToken, 
					"desc" => "Failed. Credential key error."
				);
		}
		if(strpos($snapToken,"internal server error") !== FALSE){
			$resp['header_code'] = 500;
			$resp['output'] = 
				array(
					"orderid" => $transaction_details['order_id'],
					"snapJSURL" => $midtransConf->snapJSURL, 
					"paymentPage" => $midtransConf->paymentPage . '/' . $snapToken, 
					"clientKey" => $midtransConf->clientKey, 
					"token" => $snapToken, 
					"desc" => "Failed. Something happen on our system."
				);
		}
		
		simplepay_invmodel_set::setTransaction(array(
			 'payment_gateway'		=> $midtransConf->payment_gateway
			,'app_name' 			=> (isset($params['app_name']) ? $params['app_name'] : 'COMPANY_CONFIDENTIAL')
			,'msisdn' 				=> $customer_details['phone']
			,'payment_channel' 		=> $midtransConf->payment_gateway
			,'currency' 			=> "IDR"
			,'amount' 				=> $transaction_details['gross_amount']
			,'item_id' 				=> time()
			,'item_name' 			=> "ORDER"
			,'time_request_order' 	=> date("Y-m-d H:i:s", $timestamp)
			,'time_response_order' 	=> date("Y-m-d H:i:s", time())
			,'time_confirmed_order' => ""
			,'status_code' 			=> $resp['header_code']
			,'reason_code' 			=> $snapToken
			,'merchant_transid' 	=> $transaction_details['order_id']
			,'transact_desc' 		=> $resp['output']['desc']
			,'redirect_url' 		=> ""
			,'confirm_page' 		=> ""
			,'custom' 				=> ""
		));
		
		$log->write(array('level' => 'debug', 'message' => 'Result MIDTRANS Token Snap Data : ' . json_encode($resp))); 
		
		//$resp['output'] = json_encode($resp['output']);
		
		return $resp;
	}
	
	public static function notification($params)
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . print_r($params, true)));
		
		$midtransConf = loader_config::getInstance()->getConfig('midtrans');
		
		$amount = str_replace(".00","",$params->gross_amount);
		
		$rows = simplepay_invmodel_get::getTransaction(array(
			 'payment_gateway' 		=> $midtransConf->payment_gateway
			,'currency' 			=> $params->currency
			,'amount' 				=> $amount
			,'merchant_transid' 	=> $params->order_id
		));
		
		if(count($rows) >= 1)
		{
			if ($params->transaction_status == 'capture') 
			{
				// For credit card transaction, we need to check whether transaction is challenge by FDS or not
				if ($params->payment_type == 'credit_card')
				{
					if($params->fraud_status == 'challenge')
						$mapping_desc = $midtransConf->mapNotificationDesc['capture_cc'];
					else 
						$mapping_desc = $midtransConf->mapNotificationDesc['capture'];
				}
			}
			else if ($params->transaction_status == 'settlement') $mapping_desc = $midtransConf->mapNotificationDesc['settlement'];
			else if ($params->transaction_status == 'pending') $mapping_desc = $midtransConf->mapNotificationDesc['pending'];
			else if ($params->transaction_status == 'deny') $mapping_desc = $midtransConf->mapNotificationDesc['deny'];
			else if ($params->transaction_status == 'expire') $mapping_desc = $midtransConf->mapNotificationDesc['expire'];
			else if ($params->transaction_status == 'cancel') $mapping_desc = $midtransConf->mapNotificationDesc['cancel'];
			else $mapping_desc = $midtransConf->mapNotificationDesc['error'];
			
			$mapping_desc = str_replace(
				array("@order_id@", "@type@", "@status_code@"), 
				array($params->order_id, $params->payment_type, $params->status_code), 
				$mapping_desc
			);
			
			// Save transaction Order Payment Confirmation in the database
			
			simplepay_invmodel_set::updateTransaction(array(
				 'payment_gateway' 		=> $midtransConf->payment_gateway
				,'app_name' 			=> $rows[0]['app_name']
				,'payment_channel' 		=> $params->payment_type
				,'currency' 			=> $params->currency
				,'amount' 				=> $amount
				,'time_confirmed_order' => $params->transaction_time
				,'status_code' 			=> $params->status_code
				,'reason_code' 			=> $params->transaction_status
				,'transact_desc' 		=> $mapping_desc
				,'merchant_transid' 	=> $params->order_id
				,'reserve1' 			=> "fraud_status:".$params->fraud_status
				,'reserve2' 			=> "transaction_id:".$params->transaction_id
				,'reserve3' 			=> "merchant_id:".$params->merchant_id
			));
			
			// Get Subscription data by user id & app name
			// - Adding subscription method when receive notification payment
			
			$active = 0;
			if($params->transaction_status == "capture" || $params->transaction_status == "settlement")
				$active = 1;
			
			$userid = (substr($rows[0]['userid'],0,1) == '0' ? "62" . substr($rows[0]['userid'],1,strlen($rows[0]['userid'])) : $rows[0]['userid']);
			
			$subs = simplepay_invmodel_get::getSubs(array(
				 'app_name' => $rows[0]['app_name']
				,'userid' 	=> $userid
			));
			
			//print_r($subs);die;
			if(count($subs) > 0){
				
				// Update Subscription data when subs is not empty
					
				simplepay_invmodel_set::updateSubs(array(
					 'payment_gateway' 			=> $midtransConf->payment_gateway
					,'app_name' 				=> $rows[0]['app_name']
					,'userid' 					=> $userid
					,'payment_channel' 			=> $params->payment_type
					,'currency' 				=> $params->currency
					,'amount' 					=> $amount
					,'active' 					=> $active
				));
				
			}else{
				
				// Otherwise insert into subscription table when is empty
				
				simplepay_invmodel_set::insertSubs(array(
					 'payment_gateway' 			=> $midtransConf->payment_gateway
					,'app_name' 				=> $rows[0]['app_name']
					,'userid' 					=> $userid
					,'payment_channel' 			=> $params->payment_type
					,'currency' 				=> $params->currency
					,'amount' 					=> $amount
					,'active' 					=> $active
				));
			}
			
			// Filter by app name for requirement project
			
			switch($rows[0]['app_name'])
			{
				case 'SURATSAKIT' :
					
					/* $req['body'] = implode("&", array(
						 'trx_id=' 		. $params->transaction_id
						,'currency=' 	. $params->currency
						,'amount='		. $amount
						,'msisdn='		. $userid
						,'status='		. $params->status_code
						,'datetime='	. str_replace(" ","",str_replace(":","",str_replace("-","",$params->transaction_time)))
					));

					$req['url'] = "https://suratsakit.com/action/dr-payment-api.php?";
					$req['port'] = 0;
					$req['login'] = "";
					$req['ssl'] = true;
					$req['timeout'] = 5;
					$req['headers'] = array();
					
					http_request::requestGet($req, "Order Payment"); */
					
					$req = array();
					
					$req['body'] = json_encode(array(
						 'trx_id' 	=> $params->order_id
						,'user_id'	=> $userid
						,'status'	=> $params->status_code
						,'reason' 	=> $params->transaction_status
						,'datetime' => str_replace(" ","",str_replace(":","",str_replace("-","",$params->transaction_time)))
					));

					$req['url'] = "https://suratsakit.com/action/dr-payment-api.php";
					$req['port'] = 0;
					$req['login'] = "";
					$req['ssl'] = false;
					$req['timeout'] = 5;
					$req['headers'] = array(
						'Content-Type: application/json',
						/* 'Accept-Encoding: gzip, deflate', */
						'Cache-Control: max-age=0',
						'Connection: keep-alive',
						'Accept-Language: en-US,en;q=0.8,id;q=0.6',
						'APPNAME: ' . $rows[0]['app_name'],
						'AUTH_USER: ' . $rows[0]['app_name'],
						'AUTH_PWD: '.md5('suratsakitPass2020'),
					);
					
					http_request::requestPost($req, "NOTIF MIDTRANS/SURATSAKIT");
					
				default : 
					
				break;
			}
		}
		
		return $result;
	}
}
<?php
class simplepay_module_nicepay
{	
    public static function orderPayment($params = array())
	{
		$time_request_order = date("Y-m-d H:i:s");
		
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . print_r($params, true)));
			
		$conf = loader_config::getInstance()->getConfig('nicepay');
		
		if($conf->env == "development"){
			//$iMid = $conf->merchantID_d;
			$merchantKey = $conf->merchantKey_d;
		}
		else if($conf->env == "production"){
			//$iMid = $conf->merchantID_p;
			$merchantKey = $conf->merchantKey_p;
		}
		
		$timestamp = date("YmdHis");
		$refNo = "ORD" . date("YmdHis") . str_replace('.', '', microtime(true));
		
		$amount = (isset($params['gross_amount']) ? $params['gross_amount'] : 0);
		$via = (isset($params['via']) ? $params['via'] : '');
		$bill_name = (isset($params['first_name']) ? $params['first_name'] : '') . (isset($params['last_name']) ? $params['last_name'] : '');
		$msisdn = (isset($params['phone']) ? $params['phone'] : '');
		$email = (isset($params['email']) ? $params['email'] : '');
		$userip = (isset($params['userip']) ? $params['userip'] : '');
		
		$iMid = $conf->payment_code[$via]['imid'];
		
		// SHA256(timeStamp+iMid+referenceNo+amt+merchantKey)
		$merchant_token = hash('sha256', $timestamp . $iMid . $refNo . $amount . $merchantKey);

		$an = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		
		$userSessionID = strtoupper(http_request::generateString($an, 32));
		
		if(in_array($via, $conf->cart_data_goods_qty))
		{
			$cart_data = '{"count":"1","item":[{"img_url":"http://img.aaa.com/ima1.jpg","goods_name":"Item 1 Name","goods_detail":"Item 1 Detail","goods_amt":"'.$amount.'","goods_quantity":"1"}]}';
		}
		else
		{
			$item_cart_data = array
			(
				"img_url" => "http://img.url",  
				"goods_name" => "Good Name",
				"goods_detail" => "Good Detail",
				"goods_amt" => $amount
			);
			
			$cart_data = array(
				"count" => "1",  
				"item" => $item_cart_data,  
			);
			
			$cart_data = json_encode($cart_data);
		}
		
		if(in_array($via, $conf->payment_services))
		{
			// REGISTRATION
			/* $registration_params = array(
				'timeStamp' => $timestamp,
				'iMid' => $iMid,
				'payMethod' => $conf->payment_method,
				'referenceNo' => $refNo,
				'currency' => 'IDR',
				'amt' => $amount,
				'goodsNm' => "Transaction Nicepay via " . $via,
				'billingNm' => $bill_name,
				'billingPhone' => $msisdn,
				'billingEmail' => $email,
				'billingCity' => 'Jakarta Pusat',
				'billingAddr' => 'Jalan Jenderal Gatot Subroto Kav.57',
				'billingState' => 'DKI Jakarta',
				'billingPostCd' => '10210',
				'billingCountry' => 'Indonesia',
				'deliveryNm' => $bill_name,
				'deliveryPhone' => $msisdn,
				'deliveryAddr' => 'Jalan Jenderal Gatot Subroto Kav.57',
				'deliveryCity'=>'Jakarta',
				'deliveryState'=>'DKI Jakarta',
				'deliveryPostCd'=>'12950',
				'deliveryCountry'=>'ID',
				'vat'=>'0',
				'fee'=>'0',
				'notaxAmt'=>'0',
				'description'=> "Transaction Nicepay via " . $via,
				'dbProcessUrl' => $conf->notification_url,
				'merchantToken' => $merchant_token,
				"reqDt" => date("Ymd"),
				"reqTm" => date("His"),
				'reqDomain'=> "kbgrps.com",
				'reqServerIP'=> "103.77.79.13",
				'reqClientVer'=> '',
				'userSessionID'=> $userSessionID,
				'userAgent'=>'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML,like Gecko) Chrome/60.0.3112.101 Safari/537.36',
				'userLanguage'=>'en-US',
				'userIP' => $userip,
				'mitraCd' => $conf->payment_code[$via]['mitra_code'],
				'cartData' => $cart_data,
			); */
	  
			//if($via == "DANA")
			//{
				$registration_params = array(
					'timeStamp' => $timestamp,
					'iMid' => $iMid,
					'payMethod' => $conf->payment_method,
					'referenceNo' => $refNo,
					'currency' => 'IDR',
					'amt' => $amount,
					'goodsNm' => "Transaction Nicepay via " . $via,
					'billingNm' => $bill_name,
					'billingPhone' => $msisdn,
					'billingEmail' => $email,
					'billingCity' => 'Jakarta Pusat',
					'billingAddr' => 'Jalan Jenderal Gatot Subroto Kav.57',
					'billingState' => 'DKI Jakarta',
					'billingPostCd' => '10210',
					'billingCountry' => 'Indonesia',
					'dbProcessUrl' => $conf->notification_url,
					'merchantToken' => $merchant_token,
					'mitraCd' => $conf->payment_code[$via]['mitra_code'],
					'userIP' => $userip,
					'vat' => '0',
					'fee' => '0',
					'notaxAmt' => '0',
					'description' => "Transaction Nicepay via " . $via,
					'reqDt' => date("Ymd"),
					"reqTm" => date("His"),
					'reqDomain' => 'kbgprs.com',
					'reqServerIP' =>"103.77.79.13",
					'reqClientVer' => '',
					'userSessionID' => $userSessionID,
					'userAgent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML,like Gecko) Chrome/60.0.3112.101 Safari/537.36',
					'userLanguage' => 'en-US',
					'cartData' => $cart_data
				);
			//}
			
			//print_r($registration_params);
			$req = array();
		
			$req['body'] = json_encode($registration_params);

			$req['url'] = $conf->reqURL . "/nicepay/direct/v2/registration";
			//$req['url'] = $conf->reqURL . "/nicepay/api/orderRegist.do";
			$req['port'] = 0;
			$req['login'] = "";
			$req['ssl'] = false;
			$req['timeout'] = 10;
			  
			$req['headers'] = array(
				 'Content-Type: application/json',
			);

			$resp = http_request::requestPost($req, "Registration via " . $via);
			
			if((int)$resp['header_code'] <> 0)
			{
				$rc = (int)$resp['header_code'];
				$output = json_decode($resp['output'], true);
			
				$time_response_order = date("Y-m-d H:i:s");
				
				if($output['resultCd'] == '0000')
				{
					// PAYMENT
					//https://dev.nicepay.co.id/nicepay/direct/v2/payment?timeStamp=20180302112151&tXid=TESTIDTEST01201803021122164984&merchantToken=b4171e8228be7a75d19ad29b509e76d5fc70a4c000ef87bc55cf0cda72767e72&cardNo=1234567890123456&cardExpYymm=2006&cardCvv=123&cardHolderNm=Thomas Alfa Edison&recurringToken=&preauthToken=&clickPayNo=&dataField3=&clickPayToken=&callBackUrl=https://merchant.com/callBackUrl
					
					$timestamp = date("YmdHis");
					$tXid = $output['tXid'];
					
					// SHA256(timeStamp+iMid+referenceNo+amt+merchantKey)
					$merchant_token = hash('sha256', $timestamp . $iMid . $refNo . $amount . $merchantKey);
				
					$payment_params = array(
						 "timeStamp=" . $timestamp
						,"tXid=" . $tXid
						,"merchantToken=" . $merchant_token
						,"callBackUrl=" . $conf->notification_url
					);
					
					$paid = array();
		
					$paid['body'] = implode("&", $payment_params);
					$paid['url'] = $conf->reqURL . "/nicepay/direct/v2/payment";
					//$paid['url'] = $conf->reqURL . "/nicepay/api/orderRegist.do";
					$paid['port'] = 0;
					$paid['login'] = "";
					$paid['ssl'] = false;
					$paid['timeout'] = 10;
					
					$paid['headers'] = array(
						 'Content-Type: application/x-www-form-urlencoded',
					);
			
					if(in_array($via, $conf->cart_data_goods_qty))
					{						
						ob_start();
						
						?>
						<form id="myForm" name="myForm" action="<?=$conf->reqURL . "/nicepay/direct/v2/payment"?>" method="post">
							<input type="hidden" name="timeStamp" value="<?=$timestamp?>" />
							<input type="hidden" name="tXid" value="<?=$tXid?>" />
							<input type="hidden" name="merchantToken" value="<?=$merchant_token?>" />
							<input type="hidden" name="callBackUrl" value="<?=$conf->notification_url?>" />
						</form>
						<script type="text/javascript">
							window.onload = function(){
							  document.forms['myForm'].submit();
							}
						</script>
						<?php
						
						$html_form = ob_get_clean();
						
						$filename = $tXid . ".php";
						$file = "/app/xmp2012/interface/simplepay/www/nicepay/p/" . $filename;
						
						if(!file_exists($file))
						{
							touch($file);
							
							file_put_contents($file, $html_form);
						}
						
						$headerCode = 200;
						$ack = array(
							 "redirect_url" => "http://103.77.79.13:8575/nicepay/p/".$filename
							,"trxid" => $tXid
						);
						
						$resultCode = $output['resultCd'];
						$resultMsg = $output['resultMsg'];
					}
					else
					{ 
					
						$resp = http_request::requestPost($paid, "Payment via " . $via);
						
						$headerCode = (int)$resp['header_code'];
						$response = $resp['output'];
				
						$lock_result = "/tmp/nicepay_" . $timestamp . $tXid;
						
						if(!file_exists($lock_result))
						{
							touch($lock_result);
							
							file_put_contents($lock_result, $response);
							
							$resultCode = shell_exec("sh /app/xmp2012/interface/simplepay/bin/nicepay/resultCd.sh '".$lock_result."'");
							$resultCode = str_replace("\n","",$resultCode);
							$resultCode = trim($resultCode);
							
							$resultMsg = shell_exec("sh /app/xmp2012/interface/simplepay/bin/nicepay/resultMsg.sh '".$lock_result."'");
							$resultMsg = str_replace("\n","",$resultMsg);
							$resultMsg = trim($resultMsg);
							
							unlink($lock_result);
						}
						
						//$ack = $resultCode . ':' . $resultMsg;
						
						$ack = array(
							 "status_code" => $resultCode
							,"result_msg" => $resultMsg
							,"trxid" => $tXid
						);
					
					}
					
					//$hit = http_request::requestPost ( $conf->reqURL . "/nicepay/direct/v2/payment", implode("&", $payment_params), 10 );
					//$hit = $conf->reqURL . "/nicepay/direct/v2/payment?" . implode("&", $payment_params);
					//$hit = $conf->reqURL . "/nicepay/api/orderInquiry.do?" . implode("&", $payment_params);
					
					if((int)$headerCode <> 0)
					{
						//$output = json_decode($hit, true);
						
						simplepay_invmodel_set::setTransaction(array(
							 'payment_gateway'		=> $conf->payment_gateway
							,'app_name' 			=> (isset($params['app_name']) ? $params['app_name'] : 'COMPANY_CONFIDENTIAL')
							,'msisdn' 				=> $msisdn
							,'payment_channel' 		=> $via
							,'currency' 			=> "IDR"
							,'amount' 				=> $amount
							,'item_id' 				=> $refNo
							,'item_name' 			=> "ORDER"
							,'time_request_order' 	=> $time_request_order
							,'time_response_order' 	=> $time_response_order
							,'time_confirmed_order' => date("Y-m-d H:i:s")
							,'status_code' 			=> $resultCode
							,'reason_code' 			=> $resultMsg
							,'merchant_transid' 	=> $tXid
							,'transact_desc' 		=> ""
							,'redirect_url' 		=> ""
							,'confirm_page' 		=> ""
							,'custom' 				=> ""
						));
						
						$output = array('resultCd' => $headerCode, 'output' => $ack);
					}
					else
					{
						$output = array('resultCd' => 404, 'output' => 'Error or timeout from client payment gateway');
					}
				} 
				else
				{
					simplepay_invmodel_set::setTransaction(array(
						 'payment_gateway'		=> $conf->payment_gateway
						,'app_name' 			=> (isset($params['app_name']) ? $params['app_name'] : 'COMPANY_CONFIDENTIAL')
						,'msisdn' 				=> $msisdn
						,'payment_channel' 		=> $via
						,'currency' 			=> "IDR"
						,'amount' 				=> $amount
						,'item_id' 				=> $refNo
						,'item_name' 			=> "ORDER"
						,'time_request_order' 	=> $time_request_order
						,'time_response_order' 	=> $time_response_order
						,'time_confirmed_order' => date("Y-m-d H:i:s")
						,'status_code' 			=> $output['resultCd']
						,'reason_code' 			=> $output['resultMsg']
						,'merchant_transid' 	=> $output['tXid']
						,'transact_desc' 		=> ""
						,'redirect_url' 		=> ""
						,'confirm_page' 		=> ""
						,'custom' 				=> ""
					));
					
					$output = array('resultCd' => 400, 'output' => $output['resultMsg']);
				}
			}
			else
			{
				$output = array('resultCd' => 404, 'output' => 'Error or timeout from client gateway');
			}
			
			$resp = array("header_code" => $output['resultCd'], "output" => $output);
		}
		else 
		{	
			$resp = array("header_code" => 400, "output" => "Error mitra name");
		}
		
		return $resp;
	}
	
	public static function notification($params)
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . print_r($params, true)));
		
		$conf = loader_config::getInstance()->getConfig('nicepay');
		
		$parameters = explode("&", $params);
		
		foreach($parameters as $param)
		{
			if(strpos($param, "amt=") !== FALSE)
				$amount = self::cleanNotifParam($param);
			
			if(strpos($param, "tXid=") !== FALSE)
				$trxid = self::cleanNotifParam($param);
			
			if(strpos($param, "status=") !== FALSE)
				$status = self::cleanNotifParam($param);
			
			if(strpos($param, "transDt=") !== FALSE)
				$transDt = self::cleanNotifParam($param);
				
			if(strpos($param, "transTm=") !== FALSE)
				$transTm = self::cleanNotifParam($param);
		}
		
		$transDt = substr($transDt, 0, 4) . "-" . substr($transDt, 4, 2) . "-" . substr($transDt, 6, 2);
		$transTm = substr($transTm, 0, 2) . ":" . substr($transTm, 2, 2) . ":" . substr($transTm, 4, 2);

		$transaction_time = $transDt . " " . $transTm;
		
		$rows = simplepay_invmodel_get::getTransaction(array(
			 'payment_gateway' 		=> $conf->payment_gateway
			,'currency' 			=> "IDR"
			,'amount' 				=> $amount
			,'merchant_transid' 	=> $trxid
		));
		
		if(count($rows) >= 1)
		{
			$transact_desc = $conf->payment_code[$rows[0]['payment_channel']]['status'][$status];
			
			// Save transaction Order Payment Confirmation in the database
			
			simplepay_invmodel_set::updateTransaction(array(
				 'payment_gateway' 		=> $conf->payment_gateway
				,'app_name' 			=> $rows[0]['app_name']
				,'payment_channel' 		=> $rows[0]['payment_channel']
				,'currency' 			=> "IDR"
				,'amount' 				=> $amount
				,'time_confirmed_order' => $transaction_time
				,'status_code' 			=> $status
				,'reason_code' 			=> $transact_desc
				,'transact_desc' 		=> $transact_desc
				,'merchant_transid' 	=> $trxid
				,'reserve1' 			=> ""
				,'reserve2' 			=> ""
				,'reserve3' 			=> ""
			));
			
			// Get Subscription data by user id & app name
			// - Adding subscription method when receive notification payment
			
			$active = 0;
			if(in_array((int)$status, array(0,1,2)))
				$active = 1;
			
			$userid = (substr($rows[0]['userid'],0,1) == '0' ? "62" . substr($rows[0]['userid'],1,strlen($rows[0]['userid'])) : $rows[0]['userid']);
			
			$subs = simplepay_invmodel_get::getSubs(array(
				 'app_name' => $rows[0]['app_name']
				,'userid' 	=> $userid
			));
			
			//print_r($subs);die;
			if(count($subs) > 0){
				
				// Update Subscription data when subs is not empty
					
				simplepay_invmodel_set::updateSubs(array(
					 'payment_gateway' 			=> $conf->payment_gateway
					,'app_name' 				=> $rows[0]['app_name']
					,'userid' 					=> $userid
					,'payment_channel' 			=> $rows[0]['payment_channel']
					,'currency' 				=> "IDR"
					,'amount' 					=> $amount
					,'active' 					=> $active
				));
				
			}else{
				
				// Otherwise insert into subscription table when is empty
				
				simplepay_invmodel_set::insertSubs(array(
					 'payment_gateway' 			=> $conf->payment_gateway
					,'app_name' 				=> $rows[0]['app_name']
					,'userid' 					=> $userid
					,'payment_channel' 			=> $rows[0]['payment_channel']
					,'currency' 				=> "IDR"
					,'amount' 					=> $amount
					,'active' 					=> $active
				));
			}
			
			// Filter by app name for requirement project
			
			switch($rows[0]['app_name'])
			{
				case 'SURATSAKIT' :
					
					/* $req['body'] = implode("&", array(
						 'trx_id=' 		. $params->transaction_id
						,'currency=' 	. $params->currency
						,'amount='		. $amount
						,'msisdn='		. $userid
						,'status='		. $params->status_code
						,'datetime='	. str_replace(" ","",str_replace(":","",str_replace("-","",$params->transaction_time)))
					));

					$req['url'] = "https://suratsakit.com/action/dr-payment-api.php?";
					$req['port'] = 0;
					$req['login'] = "";
					$req['ssl'] = true;
					$req['timeout'] = 5;
					$req['headers'] = array();
					
					http_request::requestGet($req, "Order Payment"); */
					
					$req = array();
					
					$req['body'] = json_encode(array(
						 'trx_id' 	=> $trxid
						,'user_id'	=> $userid
						,'status'	=> $status
						,'reason' 	=> $transact_desc
						,'datetime' => $transaction_time
					));

					$req['url'] = "https://suratsakit.com/action/dr-payment-api.php";
					$req['port'] = 0;
					$req['login'] = "";
					$req['ssl'] = false;
					$req['timeout'] = 5;
					$req['headers'] = array(
						'Content-Type: application/json',
						/* 'Accept-Encoding: gzip, deflate', */
						'Cache-Control: max-age=0',
						'Connection: keep-alive',
						'Accept-Language: en-US,en;q=0.8,id;q=0.6',
						'APPNAME: ' . $rows[0]['app_name'],
						'AUTH_USER: ' . $rows[0]['app_name'],
						'AUTH_PWD: '.md5('suratsakitPass2020'),
					);
					
					http_request::requestPost($req, "NOTIF NICEPAY/SURATSAKIT");
					
				default : 
					
				break;
			}
		}
		
		return $result;
	}
	
	private static function cleanNotifParam($d){
		
		list($prm, $curParam) = explode("=", $d);
				
		$curParam = urldecode($curParam);
		return trim($curParam);
	}
}
<?php
class simplepay_module_achiko
{
    public static function orderPayment($params = array())
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . print_r($params, true)));
			
		$configAchiko = loader_config::getInstance()->getConfig('achiko');
		
		$req = array();
		
		$datetime = date ( "Y-m-d H:i:s" );
		$timestamp = time();
        $transid = str_replace(" ", "", str_replace(":", "", str_replace("-", "", $datetime))) . str_replace ( '.', '', microtime ( true ) );
		
		// demo parameter
		$payment_channel_name = 'airtime_testing';

		$parameters = array(
             'timestamp' 				=> $timestamp
            ,'user_id' 					=> (isset($params['msisdn']) ? $params['msisdn'] : '088881234567')
            ,'merchant_transaction_id'	=> $transid
            ,'transaction_description'	=> 'Order Payment Via ' . $params['payment_channel']
            ,'payment_channel'			=> (isset($params['payment_channel']) ? $params['payment_channel'] : "airtime_testing")
            ,'currency'					=> (isset($params['currency']) ? $params['currency'] : 'IDR')
            ,'amount'					=> (isset($params['amount']) ? $params['amount'] : 10000)
            ,'item_id'					=> (isset($params['item_id']) ? $params['item_id'] : 1)
            ,'item_name'				=> (isset($params['item_name']) ? $params['item_name'] : "ORDER_REQUIREMENT_1")
            ,'redirect_url'				=> $configAchiko->redirect_url
            ,'custom'					=> ''
        );
		
        $req['body'] = json_encode($parameters);

        $req['url'] = $configAchiko->urlGateway . "/api/v1/create";
        $req['port'] = 0;
        $req['login'] = "";
        $req['ssl'] = false;
        $req['timeout'] = 5;
		
        $req['headers'] = array(
             'Content-type: application/json'
            ,'AppId: ' . $configAchiko->appId
            ,'Bodysign: ' . hash_hmac('sha256', base64_encode($req['body']), $configAchiko->secretKey)
        );

        $resp = http_request::requestPost($req, "Order Payment");
        $rc = (int)$resp['header_code'];
        
        if(!empty($resp['output']))
        {
			$output = json_decode($resp['output']);
			
			$log->write(array('level' => 'debug', 'message' => 'Result Data : ' . print_r($output, true))); 
			
			// Save transaction Order Payment in the database
			
			simplepay_invmodel_set::setTransaction(array(
				 'payment_gateway'		=> $configAchiko->payment_gateway
				,'app_name' 			=> (isset($params['app_name']) ? $params['app_name'] : 'COMPANY_CONFIDENTIAL')
				,'msisdn' 				=> $parameters['user_id']
				,'payment_channel' 		=> $parameters['payment_channel']
				,'currency' 			=> $parameters['currency']
				,'amount' 				=> $parameters['amount']
				,'item_id' 				=> $parameters['item_id']
				,'item_name' 			=> $parameters['item_name']
				,'time_request_order' 	=> date("Y-m-d H:i:s", $parameters['timestamp'])
				,'time_response_order' 	=> (isset($output->data->timestamp) ? date("Y-m-d H:i:s", $output->data->timestamp) : "")
				,'time_confirmed_order' => ""
				,'status_code' 			=> (isset($output->data->status_code) ? $output->data->status_code : $rc)
				,'reason_code' 			=> (isset($output->data->detail) ? $output->data->detail : "")
				,'merchant_transid' 	=> (isset($output->data->id) ? $output->data->id : "")
				,'transact_desc' 		=> $parameters['transaction_description']
				,'redirect_url' 		=> $configAchiko->redirect_url
				,'confirm_page' 		=> (isset($output->data->links->href) ? $output->data->links->href : "")
				,'custom' 				=> ""
			));
		}
		
		return $resp;
	}
	
	public static function orderChecking($params = array())
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . print_r($params, true)));
			
		$configAchiko = loader_config::getInstance()->getConfig('achiko');
		
		$req = array();

		$parameters = array(
             'id' 			=> (isset($params['id']) ? $params['id'] : 0)
            ,'timestamp' 	=> (isset($params['timestamp']) ? (int)$params['timestamp'] : time())
        );
		
		/* $parameters = array(
             'timestamp' 				=> (isset($params['timestamp']) ? (int)$params['timestamp'] : time())
            ,'user_id' 					=> '6287881333079'
            ,'merchant_transaction_id'	=> (isset($params['id']) ? $params['id'] : 0)
            ,'transaction_description'	=> 'Order Payment Via xl_airtime'
            ,'payment_channel'			=> 'xl_airtime'
            ,'currency'					=> 'IDR'
            ,'amount'					=> '5000'
            ,'item_id'					=> 16
            ,'item_name'				=> 'xl_airtime'
            ,'redirect_url'				=> $configAchiko->redirect_url
            ,'custom'					=> ''
        ); */
		
        $req['body'] = json_encode($parameters);

        $req['url'] = $configAchiko->urlGateway . "/api/v1/check_order";
        $req['port'] = 0;
        $req['login'] = "";
        $req['ssl'] = false;
        $req['timeout'] = 5;
		
        $req['headers'] = array(
             'Content-type: application/json'
            ,'AppId: ' . $configAchiko->appId
            ,'Bodysign: ' . hash_hmac('sha256', base64_encode($req['body']), $configAchiko->secretKey)
        );

        $resp = http_request::requestPost($req, "Order Checking");
        $rc = (int)$resp['header_code'];
        
        if(!empty($resp['output']))
        {
			$output = json_decode($resp['output']);
			
			$log->write(array('level' => 'debug', 'message' => 'Result Data : ' . print_r($output, true))); 
			
			// Save transaction Order Payment in the database
			
			/* simplepay_invmodel_set::setTransaction(array(
				 'payment_gateway'		=> $configAchiko->payment_gateway
				,'app_name' 			=> (isset($params['app_name']) ? $params['app_name'] : 'COMPANY_CONFIDENTIAL')
				,'msisdn' 				=> $parameters['user_id']
				,'payment_channel' 		=> $parameters['payment_channel']
				,'currency' 			=> $parameters['currency']
				,'amount' 				=> $parameters['amount']
				,'item_id' 				=> $parameters['item_id']
				,'item_name' 			=> $parameters['item_name']
				,'time_request_order' 	=> date("Y-m-d H:i:s", $parameters['timestamp'])
				,'time_response_order' 	=> (isset($output->data->timestamp) ? date("Y-m-d H:i:s", $output->data->timestamp) : "")
				,'time_confirmed_order' => ""
				,'status_code' 			=> (isset($output->data->status_code) ? $output->data->status_code : $rc)
				,'reason_code' 			=> (isset($output->data->detail) ? $output->data->detail : "")
				,'merchant_transid' 	=> (isset($output->data->id) ? $output->data->id : "")
				,'transact_desc' 		=> $parameters['transaction_description']
				,'redirect_url' 		=> $configAchiko->redirect_url
				,'confirm_page' 		=> (isset($output->data->links->href) ? $output->data->links->href : "")
				,'custom' 				=> ""
			)); */
		}
		
		return $resp;
	}
}
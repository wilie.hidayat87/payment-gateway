<?php
class simplepay_module_bniva
{
    public static function createBilling($params = array())
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . print_r($params, true)));
			
		$bnivaConf = loader_config::getInstance()->getConfig('bniva');

		include_once $bnivaConf->_3rdpartyPath . '/BniEnc.php';
		
		$datetime = date ( "Y-m-d H:i:s" );
		$timestamp = time();
		
		//$trx_id = str_replace(" ", "", str_replace(":", "", str_replace("-", "", $datetime))) . str_replace ( '.', '', microtime ( true ) );
		$trx_id = (isset($params['trx_id']) ? $params['trx_id'] : str_replace(" ", "", str_replace(":", "", str_replace("-", "", $datetime))) . str_replace ( '.', '', microtime ( true ) ));
		$trx_amount = (isset($params['amount']) ? $params['amount'] : 10000);
		$datetime_expired = date('c', time() + 2 * 3600); // billing will be expired in 2 hours
		$virtual_account = "";
		$cust_name = (isset($params['custname']) ? $params['custname'] : '');
		$cust_email = (isset($params['email']) ? $params['email'] : '');
		$cust_phone = (isset($params['msisdn']) ? $params['msisdn'] : 0);
		
		$raw_data = array(
			'type' => "createbilling",
			'client_id' => $bnivaConf->clientID,
			'trx_id' => $trx_id,
			'trx_amount' => $trx_amount,
			'billing_type' => "c",
			'datetime_expired' => $datetime_expired,
			'virtual_account' => '',
			'customer_name' => $cust_name,
			'customer_email' => $cust_email,
			'customer_phone' => $cust_phone,
		);

		$log->write(array('level' => 'info', 'message' => 'Raw Create Billing data - ' . serialize($raw_data)));
		
		$hashed_string = BniEnc::encrypt(
			$raw_data,
			$bnivaConf->clientID,
			$bnivaConf->secretKey
		);

		$data = array(
			'client_id' => $bnivaConf->clientID,
			'prefix' => $bnivaConf->prefix,
			'data' => $hashed_string,
		);
		
		$log->write(array('level' => 'info', 'message' => 'Hash Create Billing data - ' . serialize($data)));
		
		$req = array();
		
		$req['body'] = json_encode($data);

        $req['url'] = $bnivaConf->requestPaymentURL;
        $req['port'] = 0;
        $req['login'] = "";
        $req['ssl'] = false;
        $req['timeout'] = 5;
		
        $req['headers'] = array(
             'Content-Type: application/json',
             'Accept-Encoding: gzip, deflate',
             'Cache-Control: max-age=0',
             'Connection: keep-alive',
             'Accept-Language: en-US,en;q=0.8,id;q=0.6',
        );

        $resp = http_request::requestPost($req, "CREATE BILLING BNI VA");
        $rc = (int)$resp['header_code'];
		$output = json_decode($resp['output'], true);
		
		$data_response = BniEnc::decrypt($output['data'], $bnivaConf->clientID, $bnivaConf->secretKey);
		
		$log->write(array('level' => 'info', 'message' => 'Response Create Billing Data - ' . $resp['output'] . ', Body - ' . serialize($data_response)));
		
		simplepay_invmodel_set::setTransaction(array(
			 'closing_connection'	=> false
			,'payment_gateway'		=> $bnivaConf->payment_gateway
			,'app_name' 			=> $params['app_name']
			,'msisdn' 				=> $cust_phone
			,'payment_channel' 		=> $bnivaConf->payment_gateway
			,'currency' 			=> $params['currency']
			,'amount' 				=> $trx_amount
			,'item_id' 				=> time()
			,'item_name' 			=> "ORDER"
			,'time_request_order' 	=> date("Y-m-d H:i:s", $timestamp)
			,'time_response_order' 	=> date("Y-m-d H:i:s", time())
			,'time_confirmed_order' => ""
			,'status_code' 			=> $output['status']
			,'reason_code' 			=> ''
			,'merchant_transid' 	=> $data_response['trx_id']
			,'transact_desc' 		=> ''
			,'redirect_url' 		=> ''
			,'confirm_page' 		=> ''
			,'custom' 				=> ''
			,'reserve1'				=> 'virtualaccount:' . $data_response['virtual_account']
			,'reserve2'				=> 'custname:' . $cust_name
			,'reserve3'				=> 'custemail:' . $cust_email
		));
		
		// Filter by app name for requirement project
		switch($params['app_name'])
		{
			case 'SURATSAKIT' :
				
				$respons_data = array(
					 "trx_id"			=> $data_response['trx_id']
					,"virtual_account"	=> $data_response['virtual_account']
					,"custname"			=> $cust_name
					,"msisdn"			=> $cust_phone
					,"email"			=> $cust_email
					,"currency"			=> $params['currency']
					,"amount"			=> $trx_amount
				);
				
				$req = array();
				
				$req['body'] = json_encode(array("status" => $output['status'], "data" => $respons_data));

				$req['url'] = "https://suratsakit.com/action/dr-va-account.php";
				$req['port'] = 0;
				$req['login'] = "";
				$req['ssl'] = false;
				$req['timeout'] = 5;
				$req['headers'] = array(
					'Content-Type: application/json',
					'APPNAME: ' . $params['app_name'],
					'AUTH_USER: ' . $params['app_name'],
					'AUTH_PWD: '.md5('suratsakitPass2020'),
				);
				
				http_request::requestPost($req, "NOTIF BNIVA SURATSAKIT");
			
			break;
			
			case 'RAPIDTEST' :
				
				$respons_data = array(
					 "trx_id"			=> $data_response['trx_id']
					,"virtual_account"	=> $data_response['virtual_account']
					,"custname"			=> $cust_name
					,"msisdn"			=> $cust_phone
					,"email"			=> $cust_email
					,"currency"			=> $params['currency']
					,"amount"			=> $trx_amount
				);
				
				$req = array();
				
				$req['body'] = json_encode(array("status" => $output['status'], "data" => $respons_data));

				$req['url'] = "http://cepatsehat.net/rapid/action/dr-va-account.php";
				$req['port'] = 0;
				$req['login'] = "";
				$req['ssl'] = false;
				$req['timeout'] = 5;
				$req['headers'] = array(
					'Content-Type: application/json',
					'APPNAME: ' . $params['app_name'],
					'AUTH_USER: ' . $params['app_name'],
					'AUTH_PWD: '.md5('suratsakitPass2020'),
				);
				
				http_request::requestPost($req, "NOTIF BNIVA RAPIDTEST");
			
			break;
			
			case 'SURATCOVID' :
				
				$respons_data = array(
					 "trx_id"			=> $data_response['trx_id']
					,"virtual_account"	=> $data_response['virtual_account']
					,"custname"			=> $cust_name
					,"msisdn"			=> $cust_phone
					,"email"			=> $cust_email
					,"currency"			=> $params['currency']
					,"amount"			=> $trx_amount
				);
				
				$req = array();
				
				$req['body'] = json_encode(array("status" => $output['status'], "data" => $respons_data));

				$req['url'] = "http://suratcovid.com/app/index.php/va-bni-callback";
				$req['port'] = 0;
				$req['login'] = "";
				$req['ssl'] = false;
				$req['timeout'] = 5;
				$req['headers'] = array(
					'Content-Type: application/json',
					'APPNAME: ' . $params['app_name'],
				);
				
				http_request::requestPost($req, "NOTIF BNIVA SURATCOVID");
			
			break;
			
			default : 
				
			break;
		}
			
		return true;
	}
	
	public static function inquiryBilling($params = array())
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($params)));
			
		$bnivaConf = loader_config::getInstance()->getConfig('bniva');

		include_once $bnivaConf->_3rdpartyPath . '/BniEnc.php';
		
		$raw_data = array(
			'type' => 'inquirybilling',
			'client_id' => $bnivaConf->clientID,
			'trx_id' => $params['trx_id'],
		);

		$log->write(array('level' => 'info', 'message' => 'Raw Inquiry data - ' . serialize($raw_data)));
		
		$hashed_string = BniEnc::encrypt(
			$raw_data,
			$bnivaConf->clientID,
			$bnivaConf->secretKey
		);

		$data = array(
			'client_id' => $bnivaConf->clientID,
			'prefix' => $bnivaConf->prefix,
			'data' => $hashed_string,
		);
		
		$log->write(array('level' => 'info', 'message' => 'Hash Inquiry data - ' . serialize($data)));
		
		$req = array();
		
		$req['body'] = json_encode($data);

        $req['url'] = $bnivaConf->requestPaymentURL;
        $req['port'] = 0;
        $req['login'] = "";
        $req['ssl'] = false;
        $req['timeout'] = 5;
		
        $req['headers'] = array(
             'Content-Type: application/json',
             'Accept-Encoding: gzip, deflate',
             'Cache-Control: max-age=0',
             'Connection: keep-alive',
             'Accept-Language: en-US,en;q=0.8,id;q=0.6',
        );

        $resp = http_request::requestPost($req, "INQUIRY BNI VA");
        $rc = (int)$resp['header_code'];
		$output = json_decode($resp['output'], true);
		
		$data_response = BniEnc::decrypt($output['data'], $bnivaConf->clientID, $bnivaConf->secretKey);
		
		$log->write(array('level' => 'info', 'message' => 'Response Inquiry Data - ' . $resp['output'] . ', Body - ' . serialize($data_response)));
		
		$getStatus = json_decode($resp['output']);
		
		return array(
			 "status" => $getStatus->status
			,"data"	=> $data_response
		);
	}
}
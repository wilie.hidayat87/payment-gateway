<?php
class simplepay_module_redison
{
    public static function orderPayment($params = array())
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . print_r($params, true)));
			
		$configRedison = loader_config::getInstance()->getConfig('redison');
		
		$req = array();
		
		$datetime = date ( "Y-m-d H:i:s" );
		$timestamp = time();
		$nonce = str_replace ( '.', '', microtime ( true ) );
        $transid = str_replace(" ", "", str_replace(":", "", str_replace("-", "", $datetime))) . $nonce;
		
		// demo parameter
		$payment_channel_name = 'airtime_testing';
		
		//if($params['msisdn'] == "6285775946559") $params['amount'] = "20000";
		
		$parameters = array(
             'user_id' 					=> (isset($params['msisdn']) ? $params['msisdn'] : '088881234567')
            ,'merchant_transaction_id'	=> $transid
            ,'payment_method'			=> (isset($params['payment_channel']) ? $params['payment_channel'] : "")
            ,'currency'					=> (isset($params['currency']) ? $params['currency'] : 'IDR')
            ,'amount'					=> (isset($params['amount']) ? $params['amount'] : 5000)
            ,'item_id'					=> (isset($params['item_id']) ? $params['item_id'] : 1)
            ,'item_name'				=> (isset($params['item_name']) ? $params['item_name'] : "ORDER REQUIREMENT " . $params['item_id'])
            ,'custom'					=> ''
        );
		
        $req['body'] = json_encode($parameters);

        $req['url'] = $configRedison->createPaymentURL;
        $req['port'] = 0;
        $req['login'] = "";
        $req['ssl'] = false;
        $req['timeout'] = 5;
		
        $req['headers'] = array(
             'Content-type: application/json'
            ,'appkey: ' . $configRedison->appKey
            ,'timestamp: ' . $timestamp
            ,'nonce: ' . $nonce
            ,'secret: ' . hash('sha256', $timestamp . $nonce . $configRedison->secretKey)
			,'bodysign: ' . strtr(base64_encode( hash_hmac('sha256', $req['body'], $configRedison->secretKey, true) ), '+/', '-_')
            ,'appid: ' . $configRedison->appId
        );

        $resp = http_request::requestPost($req, "REDISON");
        $rc = (int)$resp['header_code'];
        
        if(!empty($resp['output']) && (int)$rc == 200)
        {
			$output = json_decode($resp['output']);
			
			$log->write(array('level' => 'debug', 'message' => 'Result Create Payment : ' . print_r($output, true))); 
			
			$redirect_url = str_replace("@token@", $output->data->token, str_replace("@appid@", $configRedison->appId, $configRedison->redirect_url));
			
			// Save transaction Order Payment in the database
			
			simplepay_invmodel_set::setTransaction(array(
				 'payment_gateway'		=> $configRedison->payment_gateway
				,'app_name' 			=> (isset($params['app_name']) ? $params['app_name'] : 'COMPANY_CONFIDENTIAL')
				,'msisdn' 				=> $parameters['user_id']
				,'payment_channel' 		=> $parameters['payment_method']
				,'currency' 			=> $parameters['currency']
				,'amount' 				=> $parameters['amount']
				,'item_id' 				=> $parameters['item_id']
				,'item_name' 			=> $parameters['item_name']
				,'time_request_order' 	=> date("Y-m-d H:i:s", $timestamp)
				,'time_response_order' 	=> date("Y-m-d H:i:s", time())
				,'time_confirmed_order' => ""
				,'status_code' 			=> ""
				,'reason_code' 			=> ""
				,'merchant_transid' 	=> $parameters['merchant_transaction_id']
				,'transact_desc' 		=> ""
				,'redirect_url' 		=> $redirect_url
				,'confirm_page' 		=> ""
				,'custom' 				=> ""
				,'reserve1' 			=> "token-value:" . $output->data->token
				,'reserve2' 			=> "token-codersp:" . $output->retcode
				,'reserve3' 			=> "token-msgrsp:" . $output->message
			));
			
			$ack = array(
				"output" =>
				array(
				'msisdn' 				=> $parameters['user_id']
				,'payment_channel' 		=> $parameters['payment_method']
				,'currency' 			=> $parameters['currency']
				,'amount' 				=> $parameters['amount']
				,'item_id' 				=> $parameters['item_id']
				,'item_name' 			=> $parameters['item_name']
				,'time_request_order' 	=> date("Y-m-d H:i:s", $timestamp)
				,'time_response_order' 	=> date("Y-m-d H:i:s", time())
				,'status_code' 			=> $output->retcode
				,'reason_code' 			=> $output->message
				,'merchant_transid' 	=> $parameters['merchant_transaction_id']
				,'redirect_url' 		=> $redirect_url
				),
				"header_code" => $rc
			);
		}
		else
		{
			$ack = array(
				"output" =>
				array(
				'error' => 'Error payment gateway'
				),
				"header_code" => $rc);
		}
		
		$log->write(array('level' => 'info', 'message' => 'ACK : ' . json_encode($ack)));
		
		return $ack;
	}
	
	public static function orderChecking($params = array())
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . print_r($params, true)));
			
		$configMain = loader_config::getInstance()->getConfig('main');
		
		$req = array();

		$parameters = array(
             'id' 			=> (isset($params['id']) ? $params['id'] : 0)
            ,'timestamp' 	=> (isset($params['timestamp']) ? (int)$params['timestamp'] : time())
        );
		
		/* $parameters = array(
             'timestamp' 				=> (isset($params['timestamp']) ? (int)$params['timestamp'] : time())
            ,'user_id' 					=> '6287881333079'
            ,'merchant_transaction_id'	=> (isset($params['id']) ? $params['id'] : 0)
            ,'transaction_description'	=> 'Order Payment Via xl_airtime'
            ,'payment_channel'			=> 'xl_airtime'
            ,'currency'					=> 'IDR'
            ,'amount'					=> '5000'
            ,'item_id'					=> 16
            ,'item_name'				=> 'xl_airtime'
            ,'redirect_url'				=> $configMain->redirect_url
            ,'custom'					=> ''
        ); */
		
        $req['body'] = json_encode($parameters);

        $req['url'] = $configMain->simplepay_gateway_url . "/api/v1/check_order";
        $req['port'] = 0;
        $req['login'] = "";
        $req['ssl'] = false;
        $req['timeout'] = 5;
		
        $req['headers'] = array(
             'Content-type: application/json'
            ,'AppId: ' . $configMain->appId
            ,'Bodysign: ' . hash_hmac('sha256', base64_encode($req['body']), $configMain->secretKey)
        );

        $resp = http_request::requestPost($req, "Order Checking");
        $rc = (int)$resp['header_code'];
        
        if(!empty($resp['output']))
        {
			$output = json_decode($resp['output']);
			
			$log->write(array('level' => 'debug', 'message' => 'Result Data : ' . print_r($output, true))); 
			
			// Save transaction Order Payment in the database
			
			/* simplepay_invmodel_set::setTransaction(array(
				 'payment_gateway'		=> $configMain->payment_gateway['achiko']
				,'app_name' 			=> (isset($params['app_name']) ? $params['app_name'] : 'COMPANY_CONFIDENTIAL')
				,'msisdn' 				=> $parameters['user_id']
				,'payment_channel' 		=> $parameters['payment_channel']
				,'currency' 			=> $parameters['currency']
				,'amount' 				=> $parameters['amount']
				,'item_id' 				=> $parameters['item_id']
				,'item_name' 			=> $parameters['item_name']
				,'time_request_order' 	=> date("Y-m-d H:i:s", $parameters['timestamp'])
				,'time_response_order' 	=> (isset($output->data->timestamp) ? date("Y-m-d H:i:s", $output->data->timestamp) : "")
				,'time_confirmed_order' => ""
				,'status_code' 			=> (isset($output->data->status_code) ? $output->data->status_code : $rc)
				,'reason_code' 			=> (isset($output->data->detail) ? $output->data->detail : "")
				,'merchant_transid' 	=> (isset($output->data->id) ? $output->data->id : "")
				,'transact_desc' 		=> $parameters['transaction_description']
				,'redirect_url' 		=> $configMain->redirect_url
				,'confirm_page' 		=> (isset($output->data->links->href) ? $output->data->links->href : "")
				,'custom' 				=> ""
			)); */
		}
		
		return $resp;
	}
}
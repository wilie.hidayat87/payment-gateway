<?php

class database_mysqli implements database_interfacei {

    public $resource;
    public $connProfile;
    public $numRows;

    public function connect($connProfile) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Make Connection Start : " . serialize($connProfile)));

		$this->resource = new mysqli($connProfile ['host'], $connProfile ['username'], $connProfile ['password'], $connProfile ['database']);
		
		if ($this->resource->connect_error) {
			$log->write(array('level' => 'debug', 'message' => "Resource not found : " . $this->resource->connect_error));
            return false;
		}else {
            $log->write(array('level' => 'debug', 'message' => "Resource found : " . print_r($this->resource, 1)));
        }
		
        $this->connProfile = $connProfile;
        return true;
    }
	
	public function reconnect($connProfile) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Make Connection Start : " . serialize($connProfile)));

		$this->resource = new mysqli($connProfile ['host'], $connProfile ['username'], $connProfile ['password'], $connProfile ['database']);
		
		if ($this->resource->connect_error) {
			$log->write(array('level' => 'debug', 'message' => "Resource not found : " . $this->resource->connect_error));
            return false;
		}else {
            $log->write(array('level' => 'debug', 'message' => "Resource found : " . print_r($this->resource, 1)));
        }
		
        $this->connProfile = $connProfile;
        return true;
    }

	public function check_connection($connProfile) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($connProfile)));

        $log->write(array('level' => 'debug', 'message' => "Resource : " . print_r($this->resource, 1)));
		
        if (!$this->resource->ping()) {
            $log->write(array('level' => 'info', 'message' => "Lost connection : " . $this->resource->connect_error));
            return $this->reconnect($connProfile);
        }
    }
	
    public function set_charset() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        return $this->resource->set_charset('utf8');
    }

    public function query($sql) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => "SQL : " . $sql));

        $this->check_connection($this->connProfile);

		$log->write(array('level' => 'debug', 'message' => "Resource : " . print_r($this->resource, 1)));
		
		$result = $this->resource->query($sql) or $log->write(array('level' => 'error', 'message' => "Resource not found : " . $this->resource->error));

		@mysqli_free_result($result);
		
        return true;
    }

    public function fetch($sql) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'info', 'message' => "SQL : " . $sql));
		
		$this->check_connection($this->connProfile);
		
		$log->write(array('level' => 'debug', 'message' => "Resource : " . print_r($this->resource, 1)));
		
		$result = $this->resource->query($sql) or $log->write(array('level' => 'error', 'message' => "Resource not found : " . $this->resource->error));
		
		$row2 = array();
		while($row = $result->fetch_assoc()) {
			$row2 [] = $row;
		}
	
		if (count($row2) == 0) {
            $log->write(array('level' => 'info', 'message' => "Data not found"));
        }
		
		@mysqli_free_result($result);
		
        return $row2;
    }

    public function last_insert_id() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        return $this->resource->insert_id;
    }
	
	public function closeConn() {
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
		
		return $this->resource->close();
	}

	public function filter($val) {
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Val : " . $val));
		
		return $this->resource->real_escape_string($val);
	}
}
<?php
interface database_interface {

	public function connect($connProfile);
	public function reconnect($connProfile);
	public function query($sql);
	public function fetch($sql);
	public function numRows();

}
<?php
interface database_interfacei {

	public function connect($connProfile);
	public function check_connection($connProfile);
    public function set_charset();
    public function query($sql);
    public function fetch($sql);
    public function last_insert_id();
	public function closeConn();
	
}
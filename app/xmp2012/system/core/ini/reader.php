<?php

class ini_reader {

    private static $instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance)
            self::$instance = new self ();

        return self::$instance;
    }

    public function get($obj) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($obj)));

        $iniDefault = APP_PATH . "/default/service/reply/default.ini";

        if (file_exists($obj->file)) {
            $iniFile = $obj->file;
        } else if (file_exists($iniDefault)) {
            $log->write(array('level' => 'info', 'message' => "File Doesn't Exist : " . $obj->file));
            $iniFile = $iniDefault;
        } else {
            $log->write(array('level' => 'error', 'message' => "File Doesn't Exist : " . $obj->file . " & " . $iniDefault));
            return false;
        }

        $msgIni = parse_ini_file($iniFile, true);
        if (isset($msgIni [$obj->section] [$obj->type])) {
            $reply = $msgIni [$obj->section] [$obj->type];
        } else {
            $reply = $msgIni [$obj->section] ["default"];
        }
        return $reply;
    }

}
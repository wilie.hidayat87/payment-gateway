<?php

class loader_model {

    private static $instance = NULL;
    private $model = array();
    private $connection = array();

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new self ();
        }
        return self::$instance;
    }

    public function load($modelName, $connProfile) {
        
	$log = manager_logging::getInstance();

	if (empty($modelName) || empty($connProfile)) {
            return FALSE;
        }
        $modelName = strtolower($modelName);
        $driver = loader_config::getInstance()->getConfig('database')->profile [$connProfile] ['driver'];
		
		switch($driver)
		{
			case 'mysql' : 
			case 'mysqli' : 
				
				$className = 'model_' . $modelName; 
			
			break;
			
			default :
			
				$className = 'model_' . strtolower($driver) . '_' . $modelName;
				
			break;
		}
		
        $model = $this->initModel($className);
		
		$log->write(array('level' => 'debug', 'message' => "Info : ". print_r($model, 1) . ", ClassName : " . $className));
		
		$conn = $this->initConn($connProfile);
        if (($driver == 'mysql') && ($conn === false)) {
		$log->write(array('level' => 'error', 'message' => "Connection cannot established : ".print_r($conn)));
            return false;
        }
        
        if (strtolower($driver) != 'hadoop') {
            $model->setConnection($conn);
        }

        return $model;
    }

    private function initModel($className) {
        if (!isset($this->model[$className]) || !is_object($this->model[$className])) {
            if (!class_exists($className)) {
                return FALSE;
            }
            $this->model[$className] = new $className();
	    //echo "classname = $className<br />";
		//print_r($this->model[$className]);
       // if(method_exists($this->model[$className], 'getHset')){ echo "ada"; }else{ echo "tdkada"; }
 //echo getcwd() . "\n";

	 }
        return $this->model[$className];
    }

    private function initConn($connProfile) {
        $log = manager_logging::getInstance();
        //$log->write(array('level' => 'debug', 'message' => "Check connection : " . @print_r($this->connection[$connProfile], 1)));
        $log->write(array('level' => 'debug', 'message' => "Check connection : " . $connProfile, 1));

        $db = database_base::getInstance(); // Change This to the right class
        if (!isset($this->connection[$connProfile]) && @!is_resource($this->connection[$connProfile]->resource)) {
            $conn = $db->load($connProfile, true);
            if ($conn) {
                $this->connection[$connProfile] = $conn;
            } else {
                return false;
            }
        }

        return $this->connection[$connProfile];
    }

}

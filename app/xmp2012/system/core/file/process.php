<?php

class file_process 
{
    public static function save($path, $content, $append = FALSE) {
        
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Path : " . $path . ", Data : " . $content));

		if(!file_exists($path))
		{
			touch($path);
			chmod($path, 0777);
		}
		
        $result = (boolean) file_put_contents($path, $content, ($append == TRUE) ? FILE_APPEND : FALSE);

        if (!$result && !is_writable($path)) {
            chmod($path, 0777);
        }

        if ($result === false) {
            return false;
        } else {
            return true;
        }
    }

    public static function get($pathfile)
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Path : " . $pathfile));
		
		$trxm = array();
		
		if(!file_exists($pathfile))
		{
			touch($pathfile);
			chmod($pathfile, 0777);
		}
		
		$handle = fopen($pathfile, 'r');
		
		if ($handle) 
		{
			while (($buffer = fgets($handle, 50000)) !== false) {
			   //echo $buffer . "<br />";
			   
			   //list($actual,$expected) = explode("=", trim($buffer));
			   $trxm = $buffer;
			   
			}
			if (!feof($handle)) {
				echo "Error: unexpected fgets() fail\n";
			}
			fclose($handle);
		}
		
		return $trxm;
	}
}

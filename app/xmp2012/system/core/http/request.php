<?php

class http_request {

    public static function get($url, $getfields = '', $timeout = 10) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . (!empty($getfields) ? '?' . $getfields : ""));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $output = curl_exec($ch);
        $header = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $log->write(array('level' => 'info', 'message' => "Hit url : " . $url . (!empty($getfields) ? "?" . $getfields : ""), 'response' => "Header: " . $header . ". Body: " . $output));
        return $output;
    }

    public static function post($url, $postfields, $timeout = 10, $arrCustom = array()) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

        foreach ($arrCustom as $key => $val) {
            curl_setopt($ch, $key, $val);
        }

        $output = curl_exec($ch);
        $header = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $log->write(array('level' => 'info', 'message' => "Hit url : " . $url . "?" . $postfields, 'response' => "Header: " . $header . ". Body: " . $output));
        return $output;
    }

	public static function requestGet($request, $part) {
		
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

		$start = time();
		
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request['url'] . (!empty($request['body']) ? $request['body'] : ""));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $request['headers']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $request['timeout']);
        $output = curl_exec($ch);
        $header = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

		$elapse = (int)time() - (int)$start;

		$log->write(array(
            'level' => 'info', 
            'message' => 'Request ' . $part . ' Hit URL : ' . $request['url'] . (!empty($request['body']) ? $request['body'] : "") . ', Header : ' . serialize($request['headers']), 
            'response' => 'Header: ' . $header . ', Body: ' . serialize($output) . ', Elapse: ' . $elapse
        ));
		
        return $output;
    }
	
	public static function requestPost($req, $part) {
	
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

		$start = time();
			
		$ch = @curl_init();
		@curl_setopt($ch, CURLOPT_URL, $req['url']);
		
		if($req['port'] > 0)
			@curl_setopt($ch, CURLOPT_PORT, $req['port']);
		
		if(!empty($req['login']))
		{
			@curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			@curl_setopt($ch, CURLOPT_USERPWD, $req['login']); 
		}
		
		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $req['ssl']);
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		@curl_setopt($ch, CURLOPT_TIMEOUT, $req['timeout']);
		@curl_setopt($ch, CURLOPT_POST, 1);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $req['body']);
		@curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		
		if($part == "BNI VA")
		{
			@curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			@curl_setopt($ch, CURLOPT_HEADER, false);
			@curl_setopt($ch, CURLOPT_VERBOSE, false);
			@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			@curl_setopt($ch, CURLOPT_ENCODING, true);
			@curl_setopt($ch, CURLOPT_AUTOREFERER, true);
			@curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

			@curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");
		}
		
		if(!empty($req['headers']))
			@curl_setopt($ch, CURLOPT_HTTPHEADER, $req['headers']);

		$output = @curl_exec($ch);
		$header_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$header_out = @curl_getinfo($ch, CURLINFO_HEADER_OUT);
		@curl_close($ch);

		$elapse = (int)time() - (int)$start;
		
		$resp = array();
		$resp['header_code'] = $header_code;
		$resp['output'] = $output;
		
		$log->write(array(
            'level' => 'info', 
            'message' => 'Request ' . $part . ' Hit URL : ' . $req['url'] . ', Header : ' . serialize($req['headers']) . ', Body : ' . serialize($req['body']), 
            'response' => 'Header: ' . $header_code . ', Body: ' . serialize($output) . ', Elapse: ' . $elapse
        ));
		
		return $resp;
	}
	
    public static function postBody($url, $body, $timeout = 10) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        return self::post($url, $body, $timeout);
    }

    public static function getRealIpAddr() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

	public static function generateString($input, $strength = 16) {
		$log = manager_logging::getInstance();
		
		$input_length = strlen($input);
		$random_string = '';
		for($i = 0; $i < $strength; $i++) {
			$random_character = $input[mt_rand(0, $input_length - 1)];
			$random_string .= $random_character;
		}

		return $random_string;
	}
}

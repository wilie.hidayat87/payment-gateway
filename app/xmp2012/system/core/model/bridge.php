<?php
class model_bridge extends model_base {

	public function qSql($sql = '') {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return array();
        }
    }
	
	public function eSql($sql = '') {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
		
		$result = $this->databaseObj->query($sql);
		
		return $result;
    }

	public function filter($val = '') {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
		
		return $this->databaseObj->filter($val);
    }
	
	public function closeConnection() {
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Closing Connection"));
		
		$this->databaseObj->closeConn();
		
		return true;
	}
}
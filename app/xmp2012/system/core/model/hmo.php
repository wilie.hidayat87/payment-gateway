<?php

class model_hmo extends model_base {

    public function save($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = sprintf("insert into hmo values('','%s','%s','%s','%s','%s','%s','%s')",mysql_real_escape_string($data->hset_id),mysql_real_escape_string($data->msisdn),mysql_real_escape_string($data->hash),mysql_real_escape_string($data->date_send),mysql_real_escape_string($data->time_send),mysql_real_escape_string($data->status),mysql_real_escape_string($data->closereason));
	$query = $this->databaseObj->query($sql);
        
	$log->write(array('level' => 'debug', 'message' => "Exec model_hmo SQL : " . $sql . ", Status : " . print_r($query)));

	if ($query) {
            return $this->databaseObj->last_insert_id();
        } else {
		return false;
	}
    }

    public function savePixelStorage($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = "INSERT INTO pixel_storage_".$data['partner']." (pixel,operator,servicename,date_time,is_used,partner) VALUES 
		('".mysql_real_escape_string($data['pixel'])."',
		 '".mysql_real_escape_string($data['operator'])."',
		 '".mysql_real_escape_string($data['servicename'])."',
		 '".mysql_real_escape_string($data['date_time'])."',
		 '".mysql_real_escape_string($data['is_used'])."',
		 '".mysql_real_escape_string($data['partner'])."')";
	$query = $this->databaseObj->query($sql);

        if ($query) {
            return $this->databaseObj->last_insert_id();
        } else {
		return false;
	}
    }

    public function savePixelStorageSubKeyword($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = "INSERT INTO pixel_storage (pixel,operator,servicename,date_time,is_used,partner,campurl) VALUES 
		('".mysql_real_escape_string($data['pixel'])."',
		 '".mysql_real_escape_string($data['operator'])."',
		 '".mysql_real_escape_string($data['servicename'])."',
		 '".mysql_real_escape_string($data['date_time'])."',
		 '".mysql_real_escape_string($data['is_used'])."',
		 '".mysql_real_escape_string($data['partner'])."',
		 '".mysql_real_escape_string($data['campurl'])."')";
	$query = $this->databaseObj->query($sql);

        if ($query) {
            return $this->databaseObj->last_insert_id();
        } else {
		return false;
		}
    }
	
    public function updatePixelStorage($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = "UPDATE pixel_storage_".$data['partner']." SET 
					msisdn='".mysql_real_escape_string($data['msisdn'])."',
					adn='".mysql_real_escape_string($data['adn'])."',
					channel='".mysql_real_escape_string($data['channel'])."',
					savemo='0' 
				WHERE pixel = '".mysql_real_escape_string($data['msisdn'])."' 
				AND   servicename = '".mysql_real_escape_string($data['channel'])."'
				AND	  operator = '".mysql_real_escape_string($data['operator'])."'";
				
		$query = $this->databaseObj->query($sql);

        if ($query) {
            return true;
        } else {
		return false;
	}
    }

    public function getPixel($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

	$sql2 = "SELECT id, partner FROM pixel_execute";
        $fetch2 = $this->databaseObj->fetch($sql2);
	
	$partner = "";
	if(trim($fetch2[0]['partner']) == "mobusi")
		$partner = "kimia";
	else if(trim($fetch2[0]['partner']) == "kimia")
		$partner = "mobusi";

        $sql = "SELECT pixel, partner FROM pixel_storage_".$partner." 
			WHERE is_used = '0' 
			AND left(date_time, 10) = '".date("Y-m-d")."' 
			AND servicename = '".mysql_real_escape_string($data['servicename'])."' 
			AND operator = '".mysql_real_escape_string($data['operator'])."' 
			ORDER BY date_time DESC 
			LIMIT 1";
        $fetch = $this->databaseObj->fetch($sql);
	
	ob_start();
	print_r($fetch);
	$dataFetch = ob_get_clean();

	$log = "SQL : " . $sql . " data => " . $dataFetch;
	$this->logging($log, "getPixel");

	$sql3 = "UPDATE pixel_execute SET partner = '".$partner."' WHERE id = '1'";
	$this->databaseObj->query($sql3);

        if (count($fetch) > 0) {

	    $sql2 = "UPDATE pixel_storage_".$partner." 
			SET is_used = '1' 
			WHERE pixel = '".mysql_real_escape_string($fetch[0]['pixel'])."' 
			AND partner = '".mysql_real_escape_string($fetch[0]['partner'])."' 
			AND left(date_time, 10) = '".date("Y-m-d")."' 
			AND servicename = '".mysql_real_escape_string($data['servicename'])."' 
			AND operator = '".mysql_real_escape_string($data['operator'])."'";

	    $this->databaseObj->query($sql2);

	    $log = "SQL : " . $sql2 . " pixel => " . $fetch[0]['pixel'];
	    $this->logging($log, "getPixel");

            return $fetch[0]['pixel'];
        } else {
	    
	    if($partner == "mobusi")
		$partner = "kimia";
	    else if($partner == "kimia")
		$partner = "mobusi";

	    $sql = "SELECT pixel, partner FROM pixel_storage_".$partner." 
				WHERE is_used = '0' 
				AND left(date_time, 10) = '".date("Y-m-d")."' 
				AND servicename = '".mysql_real_escape_string($data['servicename'])."' 
				AND operator = '".mysql_real_escape_string($data['operator'])."' 
				ORDER BY date_time DESC 
				LIMIT 1";
	    $fetch3 = $this->databaseObj->fetch($sql);

	    if (count($fetch3) > 0) {
		    $sql2 = "UPDATE pixel_storage_".$partner." 
				SET is_used = '1' 
				WHERE pixel = '".mysql_real_escape_string($fetch3[0]['pixel'])."' 
				AND partner = '".mysql_real_escape_string($fetch3[0]['partner'])."' 
				AND left(date_time, 10) = '".date("Y-m-d")."' 
				AND servicename = '".mysql_real_escape_string($data['servicename'])."' 
				AND operator = '".mysql_real_escape_string($data['operator'])."'";

		    $this->databaseObj->query($sql2);

		    $log = "SQL : " . $sql2 . " pixel => " . $fetch3[0]['pixel'];
		    $this->logging($log, "getPixel");
		
		    $sql4 = "UPDATE pixel_execute SET partner = '".$partner."' WHERE id = '1'";
		    $this->databaseObj->query($sql4);

		    return $fetch3[0]['pixel'];
	    }
	    else { return ""; }
        }
    }

    public function getPixelSubKeyword($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

	$id = mysql_real_escape_string($data['pixelStorageID']);
	$msisdn = mysql_real_escape_string($data['msisdn']);
        $sql = "SELECT pixel, partner FROM pixel_storage 
			WHERE is_used = '0' 
			AND id = '".$id."' 
			ORDER BY date_time DESC 
			LIMIT 1";
        $fetch = $this->databaseObj->fetch($sql);
	
		if(count($fetch) > 0)
		{
			ob_start();
			print_r($fetch);
			$dataFetch = ob_get_clean();

			$log = "SQL : " . $sql . " data => " . $dataFetch;
			$this->logging($log, "getPixel");
			
			$sql4 = "UPDATE pixel_storage SET is_used = 1, msisdn = '".$msisdn."' WHERE id = '".$id."'";
		    $this->databaseObj->query($sql4);
		}

	    return $fetch;
    }
	
    public function getStorage($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
		
		$partner = "mobusi";
		$sql = "SELECT * FROM pixel_storage_".$partner." WHERE msisdn = '".mysql_real_escape_string($data['msisdn'])."' AND savemo = '0' LIMIT 1";
        $fetch = $this->databaseObj->fetch($sql);
		
		if (count($fetch) < 1) {
			$partner = "kimia";
			$sql = "SELECT * FROM pixel_storage_".$partner." WHERE msisdn = '".mysql_real_escape_string($data['msisdn'])."' AND savemo = '0' LIMIT 1";
			$fetch = $this->databaseObj->fetch($sql);
		}
		
		$sql2 = "UPDATE pixel_storage_".$partner." SET savemo = '1' WHERE msisdn = '".mysql_real_escape_string($data['msisdn'])."'";
		$this->databaseObj->query($sql2);
	
		return $fetch;
    }

    public function isUnique($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

# query asli dibawah ini, akibatnya hanya mencari dalam waktu 24 jam, maka saya remark, biar mencari dalam seluruh data, karena hash sudah di index, oleh fajar
#        $sql = sprintf("SELECT id FROM hmo where hset_id='%s' and hash='%s' and date_send>=subdate(current_date,1) limit 1",mysql_real_escape_string($data->hset_id),mysql_real_escape_string($data->hash));
	$sql = sprintf("SELECT id FROM hmo where hset_id='%s' and hash='%s' and status='1' limit 1",mysql_real_escape_string($data->hset_id),mysql_real_escape_string($data->hash));
        $data = $this->databaseObj->fetch($sql);
        
	$log->write(array('level' => 'debug', 'message' => "Exec model_isUnique SQL : " . $sql . ", Status : " . print_r($data)));
	
	if (count($data) > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getLastSend() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

	$log->write(array('level' => 'debug', 'message' => "Exec model_getLastSend SQL : " . $sql . ", Status : " . $data));

        $sql = sprintf("SELECT hmo.id FROM hset join hmo where hset.keyword='%s' and hmo.status=1 order by hmo.id desc");
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function countLastRow($id) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = sprintf("SELECT id from hmo where hset_id = '%s' order by id desc",mysql_real_escape_string($id));
        $data = $this->databaseObj->fetch($sql);

	$log->write(array('level' => 'debug', 'message' => "Exec model_countLastRow SQL : " . $sql . ", Status : " . $data));

        if (count($data) > 0) {
            return count($data);
        } else {
            return false;
        }
    }

    public function logging($msg, $function)
    {
	//error_log("/mo/index.php  model_hmo  " . $function . "  " . date("Y-m-d H:i:s") . " " . $msg . PHP_EOL, 3, "/app/xmp2012/logs/xlsdp/cpa/cpa-" . date("Y-m-d"));
	error_log("model_hmo \t" . json_encode($function) . "  \t" . date("Y-m-d H:i:s") . "\t$msg\r\n", 3, "/app/xmp2012/logs/xlsdp/cpa/cpa-" . date("Y-m-d"));
    }
}

?>

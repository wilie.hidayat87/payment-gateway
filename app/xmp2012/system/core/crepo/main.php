<?php
class crepo_main {
	private static $instance;
	
	private function __construct() {
	}
	
	public static function getInstance() {
                $log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
                
		if (! self::$instance)
			self::$instance = new self ();
		
		return self::$instance;
	}
	public function codeExist($token) {
                $log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
                
		if ($token == false) {
			return false;
		}
		return true;
	}
}
<?php
interface summarizer_interface {
	public function execute(summarizer_data $summarizer);
}
<?php

class mail_data {

    public $smtpHost;
    public $smtpPort;
    public $smtpType;
    public $smtpUser;
    public $smtpPwd;
    public $from;
    public $sender;
    public $subject;
    public $content;

}
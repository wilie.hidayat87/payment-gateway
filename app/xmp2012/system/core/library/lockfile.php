<?php

class library_lockfile {

    protected $lockfile = '';

    public function __construct($param) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $param));

        $load_config = loader_config::getInstance();
        $config_main = $load_config->getConfig('main');
        $this->lockfile = '/tmp/lock_' . $param . '_' . $config_main->operator;
    }

    public function create($param) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $param));

        $lockFile = $this->lockfile . '_' . $param;
        if (file_exists($lockFile)) {
            echo "NOK - Lock File Exist on $lockFile \n";
            exit;
        } else {
            $log->write(array('level' => 'debug', 'message' => "Create lock : " . $lockFile));
            @touch($lockFile);
        }
        return true;
    }

    public function delete($param) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $param));

        $lockFile = $this->lockfile . '_' . $param;
        $log->write(array('level' => 'debug', 'message' => "Delete lock : " . $lockFile));
        return @unlink($lockFile);
    }

}
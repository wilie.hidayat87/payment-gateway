<?php

class content_generator_pakistan implements content_generator_interface {

    private static $instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance)
            self::$instance = new self ();

        return self::$instance;
    }

    public function generate($content_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $token = $this->getToken($content_data);

        $config = loader_config::getInstance()->getConfig('content')->urlDownload;
        $content_data->token = $token;
        $content_data->url = str_replace(array('@TOKEN@', '@CODE@'), array($content_data->token, $content_data->code), $config);
        $content_data->type = '014';

        $model_download = loader_model::getInstance()->load('download', 'connWap');
        $content_data->wapId = $model_download->getSite($content_data);
        $insert_id = $model_download->set($content_data);

        return $content_data;
    }

    private function getToken($content_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $model_session = loader_model::getInstance()->load('wapsession', 'connWap');
        $token = tools_randomize::get('4');

        $wap_session = new model_data_wapsession ();
        $wap_session->token = $token;

        if ($model_session->read($wap_session) === true)
            $this->getToken($content_data);

        return $token;
    }

}
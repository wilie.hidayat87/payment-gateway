<?php
class icd_functions {

        public $GET;

        public function safe_b64encode($string) {
                $data = base64_encode ( $string );
                $data = str_replace ( array ('+', '/', '=' ), array ('-', '_', '' ), $data );
                return $data;
        }

        public function encode($value, $key) {
                if (! $value) {
                        return false;
                }

                $text = $value;
                $iv_size = mcrypt_get_iv_size ( MCRYPT_3DES, MCRYPT_MODE_ECB );
                $iv = mcrypt_create_iv ( $iv_size, MCRYPT_RAND );
                $crypttext = mcrypt_encrypt ( MCRYPT_3DES, $key, $text, MCRYPT_MODE_ECB, $iv );

                return trim ( $this->safe_b64encode ( $crypttext ) );
        }

        public function gen_url($configIcd, $data) {
                $eData = $this->encode ( $data, $configIcd->cppass );
                $base_url = $configIcd->icd_url ['request'];
                $base_url = str_replace ( '%CPCODE%', $configIcd->cpcode, $base_url );
                $base_url = str_replace ( '%DATA%', $eData, $base_url );

                return $base_url;

        }

        public function gen_reqid($length = 5) {
                /*
            //$characters = "0123456789abcdefghijklmnopqrstuvwxyz";
            $characters = "0123456789";
            $string = date("YmdHis")." ";
            for ($p = 0; $p < $length; $p++) {
                $string .= $characters[mt_rand(0, strlen($characters))];
            }
                */
                $string = date ( 'ymdHis' ) . str_pad ( mt_rand ( 0, 99999 ), 5, '0' );
                return $string;
        }

        public function save_icd_log($data) {
                $model_icd = loader_model::getInstance ()->load ( 'icd', 'connDatabase1' );
                $insert_id = $model_icd->save_icd_log ( $data );
                if ($insert_id)
                        return true;
                else
                        return false;
        }

        public function update_icd_log($data) {
                $model_icd = loader_model::getInstance ()->load ( 'icd', 'connDatabase1' );
                return $model_icd->update_icd_log ( $data );
        }

        public function read_icd_log($data) {
                $model_icd = loader_model::getInstance ()->load ( 'icd', 'connDatabase1' );
                return $model_icd->read_icd_log ( $data );
        }

        public function read_icd_grade($data) {
                $model_icd = loader_model::getInstance ()->load ( 'icd', 'connDatabase1' );
                $icd_data = $model_icd->read_icd_grade ( $data );
                if ($icd_data)
                        return $icd_data;
                else
                        return false;
        }
}


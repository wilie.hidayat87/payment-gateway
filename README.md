Payment Gateway
======

Git global setup
======

1. git config --global user.name "Wilie Wahyu Hidayat"
2. git config --global user.email "wilie.hidayat87@gmail.com"

Link a SSH key to server
======

Check : cat ~/.ssh/id_rsa.pub

Add : ssh-keygen -t rsa -C "wilie.hidayat87@gmail.com"


Push an existing folder
======

cd /

git init

git remote add origin git@gitlab.com:wilie.hidayat87/payment-gateway.git

1. git add /app/xmp2021/interface/default/.
2. git add /app/xmp2021/interface/simplepay/.
3. git add /app/xmp2021/system/core/.

git commit -m "Initial commit"

git push -u origin master


Pull repo to production
======

git remote add origin git@gitlab.com:wilie.hidayat87/payment-gateway.git

git fetch origin

git status

git pull -u origin master

Revert pull request
======

git revert <commit sha1>

